package com.szpejsoft.zegarek.domain.model

import com.szpejsoft.zegarek.domain.model.exercises.WhatTimeItIsExercise
import org.junit.Assert
import org.junit.Test

class WhatTimeItIsExerciseTest {
    private val sample = WhatTimeItIsExercise(Instant(0, 0),
            listOf(Instant(0, 0),
                    Instant(0, 1),
                    Instant(0, 2),
                    Instant(0, 3)
            ))

    @Test
    fun twoExercisesWithSameQuestionAndAnswers_equals() {
        val exercise = WhatTimeItIsExercise(Instant(0, 0),
                listOf(Instant(0, 0),
                        Instant(0, 1),
                        Instant(0, 2),
                        Instant(0, 3)
                ))
        Assert.assertEquals(sample, exercise)
        Assert.assertEquals(sample.hashCode(), exercise.hashCode())
    }


    @Test
    fun twoExercisesWithSameQuestionDifferentAnswers_equals() {
        val exercise = WhatTimeItIsExercise(Instant(0, 0),
                listOf(Instant(0, 0),
                        Instant(0, 5),
                        Instant(1, 2),
                        Instant(3, 0)
                ))
        Assert.assertEquals(sample, exercise)
        Assert.assertEquals(sample.hashCode(), exercise.hashCode())
    }

    @Test
    fun twoExercisesWithDifferentQuestionAndSameAnswers_notEquals() {
        val exercise = WhatTimeItIsExercise(Instant(1, 17),
                listOf(Instant(0, 0),
                        Instant(0, 1),
                        Instant(0, 2),
                        Instant(0, 3)
                ))
        Assert.assertNotEquals(sample, exercise)
        Assert.assertNotEquals(sample.hashCode(), exercise.hashCode())
    }

    @Test
    fun twoExercisesWithDifferentQuestionAndDifferentAnswers_notEquals() {
        val exercise = WhatTimeItIsExercise(Instant(7, 35),
                listOf(Instant(1, 15),
                        Instant(0, 1),
                        Instant(0, 2),
                        Instant(0, 3)
                ))
        Assert.assertNotEquals(sample, exercise)
        Assert.assertNotEquals(sample.hashCode(), exercise.hashCode())
    }

}