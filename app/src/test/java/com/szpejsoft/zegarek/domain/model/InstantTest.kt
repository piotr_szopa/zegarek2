package com.szpejsoft.zegarek.domain.model

import org.junit.Assert.assertEquals
import org.junit.Test

class InstantTest {

    @Test
    fun twoHourMinutes_theSameTime_equal() {
        //given
        val hm1 = Instant(1, 15)
        val hm2 = Instant(1, 15)
        //when

        //then
        assertEquals(hm1, hm2)
    }

    @Test
    fun hourMinuteWithHourAndMinuteAboveRange_hourAndMinuteClamped() {
        //given
        val hm1 = Instant(35, 70)
        //when

        //then
        assertEquals(11, hm1.getHour())
        assertEquals(10, hm1.getMinute())
    }

    @Test
    fun twoHourMinutes_theSameTimeOneOverRange_equal() {
        //given
        val hm1 = Instant(35, 70)
        val hm2 = Instant(11, 10)
        //when

        //then
        assertEquals(hm1, hm2)
    }


}