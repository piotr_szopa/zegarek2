package com.szpejsoft.zegarek.base

import android.app.Application
import com.szpejsoft.zegarek.data.rateapp.IRateAppRepository
import com.szpejsoft.zegarek.koin.*
import org.koin.android.ext.android.inject
import org.koin.android.ext.android.startKoin
import timber.log.Timber

class ClockApplication : Application() {

    private val rateAppRepository: IRateAppRepository by inject()

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
        startKoin(this, listOf(databaseModule,
                utilsModule,
                repositoryModule,
                schedulersModule,
                useCaseModule,
                viewModelModule))
        decreaseNumberOfStartsToShowRateAppDialog()
    }

    private fun decreaseNumberOfStartsToShowRateAppDialog() {
        rateAppRepository.decreaseNumberOfStartsToNextRateAppDialog()
    }

}