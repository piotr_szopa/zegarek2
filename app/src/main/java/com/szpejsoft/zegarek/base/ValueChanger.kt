package com.szpejsoft.zegarek.base

import android.view.MotionEvent
import android.view.View
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import java.util.concurrent.TimeUnit

class ValueChanger(private val scheduler: SchedulersFacade, val action: () -> Unit) {

    private val isChanging = CompositeDisposable()

    fun startChangingValue(): Boolean {
        Observable.interval(0, 100, TimeUnit.MILLISECONDS)
                .subscribeAndObserveOnBackground(scheduler) {  action() }
                .addTo(isChanging)
        return false
    }

    fun stopChangingValue() = isChanging.clear()
}

fun View.setValueChanger(schedulersFacade: SchedulersFacade, action: () -> Unit) {
    val valueChanger = ValueChanger(schedulersFacade, action)
    this.setOnClickListener { action() }
    this.setOnLongClickListener { valueChanger.startChangingValue() }
    this.setOnTouchListener { _, event ->
        if (event.action == MotionEvent.ACTION_UP || event.action == MotionEvent.ACTION_OUTSIDE) {
            valueChanger.stopChangingValue()
        }
        return@setOnTouchListener false
    }
}

