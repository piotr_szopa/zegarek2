package com.szpejsoft.zegarek.base

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import timber.log.Timber
import java.util.*

val random: Random = Random()

fun <T> Observable<T>.timber(tag: String): Observable<T> =
        this.doOnSubscribe { Timber.d("$tag subscribed $it") }
                .doOnNext { Timber.d("$tag onNext $it") }
                .doOnError { Timber.e("$tag onError $it") }


fun <T> Single<T>.timber(tag: String): Single<T> =
        this.doOnSubscribe { Timber.d("$tag subscribed $it") }
                .doOnSuccess { Timber.d("$tag onNext $it") }
                .doOnError { Timber.e("$tag onError $it") }

fun <T> Observable<T>.subscribeAndObserveOnBackground(schedulers: SchedulersFacade,
                                                      onError: (Throwable) -> Unit = (Timber::w),
                                                      onComplete: () -> Unit = { },
                                                      onNext: (T) -> Unit
): Disposable = subscribeOn(schedulers.background())
        .observeOn(schedulers.background())
        .subscribe(onNext, onError, onComplete)