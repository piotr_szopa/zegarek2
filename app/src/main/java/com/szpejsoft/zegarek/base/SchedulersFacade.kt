package com.szpejsoft.zegarek.base

import io.reactivex.Scheduler

interface SchedulersFacade {

    fun background(): Scheduler

    fun ui(): Scheduler
}
