package com.szpejsoft.zegarek.domain.model

import android.content.Context
import android.graphics.Typeface
import android.text.format.DateUtils
import androidx.annotation.FontRes
import androidx.annotation.StringRes
import androidx.annotation.StyleRes
import androidx.core.content.res.ResourcesCompat
import com.szpejsoft.zegarek.R
import com.szpejsoft.zegarek.base.random
import java.util.*

fun Period.getLocalizedStringOneLine(context: Context): String {
    val hours = context.resources.getQuantityString(R.plurals.hourPlurals, this.hours, this.hours)
    val minutes = context.resources.getQuantityString(R.plurals.minutePlurals, this.minutes, this.minutes)
    return context.getString(R.string.period_breakable, hours, minutes)
}

fun Period.getLocalizedStringTwoLines(context: Context): String {
    val hours = context.resources.getQuantityString(R.plurals.hourPlurals, this.hours, this.hours)
    val minutes = context.resources.getQuantityString(R.plurals.minutePlurals, this.minutes, this.minutes)
    return context.getString(R.string.period_two_lines, hours, minutes)
}

fun Instant.getLocalizedTime(context: Context): String {
    val formatter = android.text.format.DateFormat.getTimeFormat(context)
    formatter.timeZone = TimeZone.getTimeZone("GMT")
    val millis = this.getMinute() * DateUtils.MINUTE_IN_MILLIS + this.getHour() * DateUtils.HOUR_IN_MILLIS
    val time = formatter.format(Date(millis)).replace(" ", "\u00A0")
    return if (time.startsWith("0")) time.substring(1) else time
}

fun Instant.howMuchTimePassedFrom(before: Instant): Period {
    assert(this >= before)
    val passedMinutes = (this.getHour() - before.getHour()) * 60 + this.getMinute() - before.getMinute()
    return Period(passedMinutes / 60, passedMinutes % 60)
}

fun Instant.add(period: Period): Instant {
    val minutes = this.getMinute() + period.minutes
    val hours = this.getHour() + period.hours + minutes / 60
    return Instant(hours, minutes % 60)
}


fun Level.createInstant(): Instant = when (this) {
    Level.EASY -> Instant(random.nextInt(12), 15 * random.nextInt(4))
    Level.MEDIUM -> Instant(random.nextInt(24), 5 * random.nextInt(12))
    Level.HARD -> Instant(random.nextInt(24), random.nextInt(60))
}

fun Level.createPeriod(): Period = when (this) {
    Level.EASY -> Period(random.nextInt(12), 15 * random.nextInt(4))
    Level.MEDIUM -> Period(random.nextInt(24), 5 * random.nextInt(12))
    Level.HARD -> Period(random.nextInt(24), random.nextInt(60))
}

fun Level.getAdjustmentPrecision(): AdjustmentPrecision = when (this) {
    Level.EASY, Level.MEDIUM -> AdjustmentPrecision.FIVE
    Level.HARD -> AdjustmentPrecision.ONE
}

@StringRes
fun Level.getNameResId(): Int = when (this) {
    Level.EASY -> R.string.level_name_easy
    Level.MEDIUM -> R.string.level_name_medium
    Level.HARD -> R.string.level_name_hard
}

@StringRes
fun ExerciseMode.getNameResId(): Int = when (this) {
    ExerciseMode.LEARN -> R.string.exercise_mode_learn
    ExerciseMode.TEST -> R.string.exercise_mode_test
}

@StringRes
fun Font.getNameResId(): Int = when (this) {
    Font.ROBOTO -> R.string.font_name_roboto
    Font.OPEN_DYSLEXIC -> R.string.font_name_open_dyslexic
    Font.CADMAN -> R.string.font_name_cadman
}

@FontRes
fun Font.getFontResId(): Int = when (this) {
    Font.ROBOTO -> R.font.roboto
    Font.OPEN_DYSLEXIC -> R.font.open_dyslexic
    Font.CADMAN -> R.font.cadman
}

@StyleRes
fun Font.getThemeResId(): Int = when (this) {
    Font.ROBOTO -> R.style.Roboto
    Font.OPEN_DYSLEXIC -> R.style.OpenDyslexic
    Font.CADMAN -> R.style.Cadman
}

fun Font.getTypeface(context: Context): Typeface =
        when (this) {
            Font.ROBOTO -> ResourcesCompat.getFont(context, R.font.roboto)!!
            Font.OPEN_DYSLEXIC -> ResourcesCompat.getFont(context, R.font.open_dyslexic)!!
            Font.CADMAN -> ResourcesCompat.getFont(context, R.font.cadman)!!
        }

@StringRes
fun NumeralsType.getNameResId(): Int = when (this) {
    NumeralsType.ARABIC_NUMERALS -> R.string.numerals_arabic_name
    NumeralsType.ROMAN_NUMERALS -> R.string.numerals_roman_name
}

