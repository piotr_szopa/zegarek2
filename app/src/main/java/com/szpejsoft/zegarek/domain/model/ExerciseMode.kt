package com.szpejsoft.zegarek.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class ExerciseMode : Parcelable {
    LEARN, TEST;
}