package com.szpejsoft.zegarek.domain.usecase.whattimeitis

import com.szpejsoft.zegarek.data.exercises.IExercisesNumberRepository
import com.szpejsoft.zegarek.data.font.IFontRepository
import com.szpejsoft.zegarek.data.level.ILevelRepository
import com.szpejsoft.zegarek.data.numerals.INumeralsRepository
import com.szpejsoft.zegarek.data.whattimeitis.IWhatTimeItIsRepository
import com.szpejsoft.zegarek.domain.model.*
import com.szpejsoft.zegarek.domain.model.exercises.WhatTimeItIsExercise
import com.szpejsoft.zegarek.domain.model.exercises.ExercisesSettings
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function3

class WhatTimeItIsUseCase(private val whatTimeItIsRepository: IWhatTimeItIsRepository,
                          private val levelRepository: ILevelRepository,
                          private val fontRepository: IFontRepository,
                          private val numeralsRepository: INumeralsRepository,
                          private val exercisesNumberRepository: IExercisesNumberRepository
) : IWhatTimeItIsUseCase {

    override fun getExercises(): Single<List<WhatTimeItIsExercise>> {
        val levelSingle = levelRepository
                .observeLevel
                .first(Level.EASY)
        val exercisesNumberSingle = exercisesNumberRepository.getExercisesNumberSettings()

        return Single.zip(levelSingle, exercisesNumberSingle,
                BiFunction<Level, ExercisesNumberSettings, ExercisesSettings> { level, exercisesSettings ->
                    ExercisesSettings(exercisesSettings.current, level)
                }).flatMap { settings -> whatTimeItIsRepository.getExercises(settings.exercisesNumber, settings.level) }
    }

    override fun getClockSettings(): Single<ClockSettings> {
        val fontSingle = fontRepository.fontObservable.first(Font.ROBOTO)
        val levelSingle = levelRepository.observeLevel.first(Level.EASY).cache()
        val numeralsSingle = numeralsRepository.getNumeralsType()

        val shownDigitsSingle = levelSingle.map {
            when (it) {
                Level.EASY, Level.MEDIUM -> ShownDigits.ALL
                Level.HARD -> ShownDigits.EVERY_THIRD
            }
        }

        return Single.zip(fontSingle, shownDigitsSingle, numeralsSingle,
                Function3<Font, ShownDigits, NumeralsType, ClockSettings> { font, shownDigits, numeralsType ->
                    ClockSettings(font, AdjustmentPrecision.FIVE, shownDigits, numeralsType, false)
                })
    }

}