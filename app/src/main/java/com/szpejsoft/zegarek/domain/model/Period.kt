package com.szpejsoft.zegarek.domain.model

data class Period(val hours: Int, val minutes: Int) : Comparable<Period> {

    init {
        assert(hours >= 0 && minutes >= 0 && minutes < 60)
    }

    override fun toString() = "${hours}h${minutes}m"

    override fun compareTo(other: Period): Int {
        val thisDuration = hours * 60 + minutes
        val otherDuration = other.hours * 60 + minutes
        return Integer.compare(thisDuration, otherDuration)
    }
}