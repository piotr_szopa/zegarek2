package com.szpejsoft.zegarek.domain.model.exercises

import com.szpejsoft.zegarek.domain.model.Instant
import com.szpejsoft.zegarek.domain.model.Period

data class WhatTimeWillBeExercise(val startTime: Instant,
                                  val difference: Period,
                                  val answers: List<Instant>) {
    init {
        assert(answers.size == 4)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as WhatTimeWillBeExercise

        if (startTime != other.startTime) return false
        if (difference != other.difference) return false

        return true
    }

    override fun hashCode(): Int {
        var result = startTime.hashCode()
        result = 31 * result + difference.hashCode()
        return result
    }

}