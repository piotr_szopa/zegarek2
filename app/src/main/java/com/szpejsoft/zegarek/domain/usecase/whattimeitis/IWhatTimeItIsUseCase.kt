package com.szpejsoft.zegarek.domain.usecase.whattimeitis

import com.szpejsoft.zegarek.domain.model.ClockSettings
import com.szpejsoft.zegarek.domain.model.exercises.WhatTimeItIsExercise
import io.reactivex.Single

interface IWhatTimeItIsUseCase {
    fun getExercises(): Single<List<WhatTimeItIsExercise>>
    fun getClockSettings(): Single<ClockSettings>
}