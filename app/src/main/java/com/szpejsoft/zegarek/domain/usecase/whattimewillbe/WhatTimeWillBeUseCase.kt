package com.szpejsoft.zegarek.domain.usecase.whattimewillbe

import com.szpejsoft.zegarek.data.exercises.IExercisesNumberRepository
import com.szpejsoft.zegarek.data.level.ILevelRepository
import com.szpejsoft.zegarek.data.whattimewillbe.IWhatTimeWillBeRepository
import com.szpejsoft.zegarek.domain.model.ExercisesNumberSettings
import com.szpejsoft.zegarek.domain.model.Level
import com.szpejsoft.zegarek.domain.model.exercises.WhatTimeWillBeExercise
import com.szpejsoft.zegarek.domain.model.exercises.ExercisesSettings
import io.reactivex.Single
import io.reactivex.functions.BiFunction

class WhatTimeWillBeUseCase(private val whatTimeWillBeRepository: IWhatTimeWillBeRepository,
                            private val levelRepository: ILevelRepository,
                            private val exercisesNumberRepository: IExercisesNumberRepository

) : IWhatTimeWillBeUseCase {
    override fun getExercises(): Single<List<WhatTimeWillBeExercise>> {
        val levelSingle = levelRepository
                .observeLevel
                .first(Level.EASY)
        val exercisesNumberSingle = exercisesNumberRepository
                .getExercisesNumberSettings()

        return Single.zip(levelSingle, exercisesNumberSingle,
                BiFunction<Level, ExercisesNumberSettings, ExercisesSettings> { level, exercisesSettings ->
                    ExercisesSettings(exercisesSettings.current, level)
                }).flatMap { settings ->
            whatTimeWillBeRepository.getExercises(settings.exercisesNumber, settings.level)
        }
    }

}
