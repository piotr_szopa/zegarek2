package com.szpejsoft.zegarek.domain.model.exercises

import com.szpejsoft.zegarek.domain.model.Instant
import com.szpejsoft.zegarek.domain.model.Period
import com.szpejsoft.zegarek.domain.model.howMuchTimePassedFrom

data class TimePassedExercise(val startTime: Instant,
                              val endTime: Instant,
                              val answers: List<Period>) {

    val properAnswer: Period
        get() = endTime.howMuchTimePassedFrom(startTime)

    init {
        assert(answers.size == 4)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TimePassedExercise

        if (startTime != other.startTime) return false
        if (endTime != other.endTime) return false

        return true
    }

    override fun hashCode(): Int {
        var result = startTime.hashCode()
        result = 31 * result + endTime.hashCode()
        return result
    }
}