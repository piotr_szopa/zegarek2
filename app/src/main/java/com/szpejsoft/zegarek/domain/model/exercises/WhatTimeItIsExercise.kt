package com.szpejsoft.zegarek.domain.model.exercises

import com.szpejsoft.zegarek.domain.model.Instant

class WhatTimeItIsExercise(val question: Instant, val answers: List<Instant>) {
    init {
        assert(answers.size == 4)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as WhatTimeItIsExercise

        if (question != other.question) return false

        return true
    }

    override fun hashCode(): Int {
        return question.hashCode()
    }
}