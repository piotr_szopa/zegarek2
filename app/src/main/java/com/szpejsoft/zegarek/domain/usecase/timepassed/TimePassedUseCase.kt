package com.szpejsoft.zegarek.domain.usecase.timepassed

import com.szpejsoft.zegarek.data.exercises.IExercisesNumberRepository
import com.szpejsoft.zegarek.data.level.ILevelRepository
import com.szpejsoft.zegarek.data.timepassed.ITimePassedRepository
import com.szpejsoft.zegarek.domain.model.ExercisesNumberSettings
import com.szpejsoft.zegarek.domain.model.Level
import com.szpejsoft.zegarek.domain.model.exercises.TimePassedExercise
import com.szpejsoft.zegarek.domain.model.exercises.ExercisesSettings
import io.reactivex.Single
import io.reactivex.functions.BiFunction

class TimePassedUseCase(private val levelRepository: ILevelRepository,
                        private val exercisesNumberRepository: IExercisesNumberRepository,
                        private val timePassedRepository: ITimePassedRepository) : ITimePassedUseCase {
    override fun getExercises(): Single<List<TimePassedExercise>> {
        val exercisesNumberSingle = exercisesNumberRepository.getExercisesNumberSettings()
        val levelSingle = levelRepository.observeLevel.first(Level.EASY)

        return Single.zip(exercisesNumberSingle, levelSingle, BiFunction<ExercisesNumberSettings, Level, ExercisesSettings> { exercisesNumberSettings, level ->
            ExercisesSettings(exercisesNumberSettings.current, level)
        }).flatMap { settings -> timePassedRepository.getExercises(settings.exercisesNumber, settings.level) }
    }
}