package com.szpejsoft.zegarek.domain.model.exercises

import com.szpejsoft.zegarek.domain.model.Level

class ExercisesSettings(val exercisesNumber: Int, val level: Level)