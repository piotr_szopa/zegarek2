package com.szpejsoft.zegarek.domain.model.exercises

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class ChosenExercise : Parcelable {
    WHAT_TIME_IT_IS, ADJUST_CLOCK, TIME_PASSED, WHAT_TIME_WILL_BE
}