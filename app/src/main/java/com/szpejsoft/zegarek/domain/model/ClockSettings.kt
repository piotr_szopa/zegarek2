package com.szpejsoft.zegarek.domain.model

class ClockSettings(val font: Font,
                    val adjustmentPrecision: AdjustmentPrecision,
                    val shownDigits: ShownDigits,
                    val numeralsType: NumeralsType,
                    val isAdjustmentMode: Boolean)

enum class ShownDigits(val step: Int) {
    EVERY_THIRD(3), ALL(1)
}

enum class AdjustmentPrecision(val step: Int) {
    ONE(1), FIVE(5)
}
