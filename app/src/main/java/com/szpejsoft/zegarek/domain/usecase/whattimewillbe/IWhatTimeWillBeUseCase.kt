package com.szpejsoft.zegarek.domain.usecase.whattimewillbe

import com.szpejsoft.zegarek.domain.model.exercises.WhatTimeWillBeExercise
import io.reactivex.Single

interface IWhatTimeWillBeUseCase {
    fun getExercises(): Single<List<WhatTimeWillBeExercise>>
}