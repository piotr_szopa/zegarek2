package com.szpejsoft.zegarek.domain.model

enum class Level {
    EASY, MEDIUM, HARD;
}