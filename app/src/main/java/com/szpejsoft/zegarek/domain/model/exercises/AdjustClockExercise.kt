package com.szpejsoft.zegarek.domain.model.exercises

import com.szpejsoft.zegarek.domain.model.Instant

data class AdjustClockExercise(val question: Instant, val initialTime: Instant) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as AdjustClockExercise

        if (question != other.question) return false

        return true
    }

    override fun hashCode(): Int {
        return question.hashCode()
    }
}