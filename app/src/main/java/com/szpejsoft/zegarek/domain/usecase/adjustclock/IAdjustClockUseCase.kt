package com.szpejsoft.zegarek.domain.usecase.adjustclock

import com.szpejsoft.zegarek.domain.model.exercises.AdjustClockExercise
import com.szpejsoft.zegarek.domain.model.ClockSettings
import io.reactivex.Single

interface IAdjustClockUseCase {
    fun getExercises(): Single<List<AdjustClockExercise>>
    fun getClockSettings(): Single<ClockSettings>
}