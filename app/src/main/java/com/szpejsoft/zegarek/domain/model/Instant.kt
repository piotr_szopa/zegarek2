package com.szpejsoft.zegarek.domain.model

class Instant(private val hour: Int = 0, private val minute: Int = 0) : Comparable<Instant> {

    fun getHour() = (hour % 24 + 24) % 24

    fun getMinute() = (minute % 60 + 60) % 60

    override fun compareTo(other: Instant): Int {
        val passedMinutes = (this.getHour() - other.getHour()) * 60 +
                this.getMinute() - other.getMinute()
        return Integer.compare(passedMinutes, 0)
    }

    override fun hashCode(): Int {
        var result = getHour()
        result = 31 * result + getMinute()
        return result
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Instant

        if (getHour() != other.getHour()) return false
        if (getMinute() != other.getMinute()) return false

        return true
    }

    override fun toString(): String = "$hour:$minute"

    fun equalsMod12(other: Instant): Boolean {
        if (getMinute() != other.getMinute()) return false
        return (getHour() - other.getHour()) % 12 == 0
    }



}
