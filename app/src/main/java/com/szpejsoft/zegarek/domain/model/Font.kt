package com.szpejsoft.zegarek.domain.model

enum class Font {
    ROBOTO, OPEN_DYSLEXIC, CADMAN;
}