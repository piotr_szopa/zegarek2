package com.szpejsoft.zegarek.domain.model

enum class NumeralsType(val numerals: Array<String>) {
    ARABIC_NUMERALS(arrayOf<String>("12", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11")), //
    ROMAN_NUMERALS(arrayOf<String>("XII", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI"));
}