package com.szpejsoft.zegarek.domain.model

enum class AnswerEvaluation {
    SUCCESS, FAILURE
}