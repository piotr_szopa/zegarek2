package com.szpejsoft.zegarek.domain.usecase.timepassed

import com.szpejsoft.zegarek.domain.model.exercises.TimePassedExercise
import io.reactivex.Single

interface ITimePassedUseCase {
    fun getExercises(): Single<List<TimePassedExercise>>
}