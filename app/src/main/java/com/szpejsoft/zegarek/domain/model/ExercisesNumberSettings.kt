package com.szpejsoft.zegarek.domain.model

class ExercisesNumberSettings(val current: Int, val minimum: Int = 1, val maximum: Int = 10) {
    init {
        require(current in minimum..maximum) { "current must be in [minimum, maximum]" }
    }
}