package com.szpejsoft.zegarek.domain.usecase.adjustclock

import com.szpejsoft.zegarek.data.adjustclock.IAdjustClockRepository
import com.szpejsoft.zegarek.data.exercises.IExercisesNumberRepository
import com.szpejsoft.zegarek.data.font.IFontRepository
import com.szpejsoft.zegarek.data.level.ILevelRepository
import com.szpejsoft.zegarek.data.numerals.INumeralsRepository
import com.szpejsoft.zegarek.domain.model.*
import com.szpejsoft.zegarek.domain.model.exercises.AdjustClockExercise
import com.szpejsoft.zegarek.domain.model.exercises.ExercisesSettings
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function4

class AdjustClockUseCase(private val adjustClockRepository: IAdjustClockRepository,
                         private val levelRepository: ILevelRepository,
                         private val fontRepository: IFontRepository,
                         private val numeralsRepository: INumeralsRepository,
                         private val exercisesNumberRepository: IExercisesNumberRepository) : IAdjustClockUseCase {
    override fun getExercises(): Single<List<AdjustClockExercise>> {
        val levelSingle = levelRepository
                .observeLevel
                .first(Level.EASY)
        val exercisesNumberSingle = exercisesNumberRepository.getExercisesNumberSettings()

        return Single.zip(levelSingle, exercisesNumberSingle,
                BiFunction<Level, ExercisesNumberSettings, ExercisesSettings> { level, exercisesSettings ->
                    ExercisesSettings(exercisesSettings.current, level)
                }).flatMap { settings -> adjustClockRepository.getExercises(settings.exercisesNumber, settings.level) }
    }

    override fun getClockSettings(): Single<ClockSettings> {
        val fontSingle = fontRepository.fontObservable.first(Font.ROBOTO)
        val levelSingle = levelRepository.observeLevel.first(Level.EASY).cache()
        val numeralsSingle = numeralsRepository.getNumeralsType()

        val adjustmentPrecisionSingle = levelSingle.map { it.getAdjustmentPrecision() }
        val shownDigitsSingle = levelSingle.map {
            when (it) {
                Level.EASY, Level.MEDIUM -> ShownDigits.ALL
                Level.HARD -> ShownDigits.EVERY_THIRD
            }
        }

        return Single.zip(fontSingle, adjustmentPrecisionSingle, shownDigitsSingle, numeralsSingle,
                Function4<Font, AdjustmentPrecision, ShownDigits, NumeralsType, ClockSettings>
                { font, adjustmentPrecision, shownDigits, numeralsType ->
                    ClockSettings(font, adjustmentPrecision, shownDigits, numeralsType, true)
                })
    }

}