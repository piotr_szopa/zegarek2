package com.szpejsoft.zegarek.data.numerals

import com.szpejsoft.zegarek.domain.model.NumeralsType
import io.reactivex.Observable
import io.reactivex.Single

class NumeralsRepository(private val numeralsDao: INumeralsDao) : INumeralsRepository {
    override val numeralsTypeObservable: Observable<NumeralsType> = numeralsDao.numeralsTypeObservable

    override fun getNumeralsType(): Single<NumeralsType> = numeralsDao.numeralsTypeObservable.first(NumeralsType.ARABIC_NUMERALS)

    override fun saveNumeralsType(numeralsType: NumeralsType) {
        numeralsDao.saveNumeralsTypeObservable(numeralsType)
    }
}