package com.szpejsoft.zegarek.data.music

import io.reactivex.Observable

interface IMusicDao{
    val musicStateObservable: Observable<Boolean>
    val soundStateObservable: Observable<Boolean>

    fun saveMusicState(musicOn: Boolean)
    fun saveSoundState(soundOn: Boolean)
}