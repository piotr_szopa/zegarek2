package com.szpejsoft.zegarek.data.numerals

import com.szpejsoft.zegarek.domain.model.NumeralsType
import io.reactivex.Observable

interface INumeralsDao {
    val numeralsTypeObservable: Observable<NumeralsType>
    fun saveNumeralsTypeObservable(type: NumeralsType)

}