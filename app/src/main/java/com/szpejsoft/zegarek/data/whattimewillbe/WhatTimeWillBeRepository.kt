package com.szpejsoft.zegarek.data.whattimewillbe

import com.szpejsoft.zegarek.domain.model.*
import com.szpejsoft.zegarek.domain.model.exercises.WhatTimeWillBeExercise
import io.reactivex.Single

class WhatTimeWillBeRepository : IWhatTimeWillBeRepository {
    override fun getExercises(howMany: Int, level: Level): Single<List<WhatTimeWillBeExercise>> {
        val exercises = mutableSetOf<WhatTimeWillBeExercise>()
        while (exercises.size < howMany) {
            exercises.add(getExercise(level))
        }
        return Single.just(exercises.toList().shuffled())
    }

    private fun getExercise(level: Level): WhatTimeWillBeExercise {
        val startTime = level.createInstant()
        val difference = level.createPeriod()

        val answer = getProperAnswer(startTime, difference)
        val answers = mutableSetOf(answer)
        while (answers.size < 4) {
            answers.add(level.createInstant())
        }
        return WhatTimeWillBeExercise(startTime, difference, answers.toList().shuffled())
    }

    private fun getProperAnswer(startTime: Instant, difference: Period): Instant {
        val minutes = (startTime.getHour() + difference.hours) * 60 +
                startTime.getMinute() + difference.minutes
        return Instant(minutes / 60, minutes % 60)
    }
}