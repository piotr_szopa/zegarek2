package com.szpejsoft.zegarek.data.music

import io.reactivex.Observable

class MusicRepository(private val musicDao: IMusicDao) : IMusicRepository {

    override val musicStateObservable: Observable<Boolean> = musicDao.musicStateObservable
    override val soundStateObservable: Observable<Boolean> = musicDao.soundStateObservable

    override fun saveMusicState(musicOn: Boolean) = musicDao.saveMusicState(musicOn)
    override fun saveSoundState(soundOn: Boolean) = musicDao.saveSoundState(soundOn)

}