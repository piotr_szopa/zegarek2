package com.szpejsoft.zegarek.data.music

import com.szpejsoft.zegarek.data.common.IClockDB

class MusicDao(private val clockDB: IClockDB) : IMusicDao {
    override val musicStateObservable = clockDB.musicStateObservable
    override val soundStateObservable = clockDB.soundStateObservable


    override fun saveMusicState(musicOn: Boolean) {
        clockDB.saveMusicState(musicOn)
    }

    override fun saveSoundState(soundOn: Boolean) {
        clockDB.saveSoundState(soundOn)
    }
}