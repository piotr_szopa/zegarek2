package com.szpejsoft.zegarek.data.rateapp

import io.reactivex.Observable


interface IRateAppDao {
    val showRateAppDialogObservable: Observable<Boolean>
    val numberOfStartsToShowRateAppDialog: Observable<Int>

    fun saveShowRateAppDialog(show: Boolean)
    fun decreaseNumberOfStartsToNextRateAppDialog()
}