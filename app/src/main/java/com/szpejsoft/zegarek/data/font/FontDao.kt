package com.szpejsoft.zegarek.data.font

import com.szpejsoft.zegarek.data.common.IClockDB
import com.szpejsoft.zegarek.domain.model.Font
import io.reactivex.Observable

class FontDao(private val clockDB: IClockDB) : IFontDao {
    override val fontObservable: Observable<Font> = clockDB.fontObservable

    override fun saveFont(font: Font) {
        clockDB.saveFont(font)
    }
}