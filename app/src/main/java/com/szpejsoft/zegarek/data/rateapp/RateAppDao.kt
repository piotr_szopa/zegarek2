package com.szpejsoft.zegarek.data.rateapp

import com.szpejsoft.zegarek.data.common.IClockDB
import io.reactivex.Observable

class RateAppDao(private val clockDB: IClockDB) : IRateAppDao {
    override val showRateAppDialogObservable: Observable<Boolean> = clockDB.showRateAppDialogObservable

    override val numberOfStartsToShowRateAppDialog: Observable<Int> = clockDB.numberOfStartsToShowRateAppDialog


    override fun saveShowRateAppDialog(show: Boolean) {
        clockDB.setShowRateAppDialog(show)
    }

    override fun decreaseNumberOfStartsToNextRateAppDialog() {
        clockDB.decreaseNumberOfStartsToNextRateAppDialog()
    }
}