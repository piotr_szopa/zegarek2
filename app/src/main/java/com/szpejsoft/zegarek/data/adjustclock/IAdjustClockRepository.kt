package com.szpejsoft.zegarek.data.adjustclock

import com.szpejsoft.zegarek.domain.model.exercises.AdjustClockExercise
import com.szpejsoft.zegarek.domain.model.Level
import io.reactivex.Single


interface IAdjustClockRepository {
    fun getExercises(howMany: Int, level: Level): Single<List<AdjustClockExercise>>
}