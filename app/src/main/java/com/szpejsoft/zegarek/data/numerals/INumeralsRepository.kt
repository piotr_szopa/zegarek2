package com.szpejsoft.zegarek.data.numerals

import com.szpejsoft.zegarek.domain.model.NumeralsType
import io.reactivex.Observable
import io.reactivex.Single

interface INumeralsRepository {
    val numeralsTypeObservable: Observable<NumeralsType>

    fun getNumeralsType(): Single<NumeralsType>
    fun saveNumeralsType(numeralsType: NumeralsType)
}