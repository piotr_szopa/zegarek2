package com.szpejsoft.zegarek.data.level

import com.szpejsoft.zegarek.data.common.IClockDB
import com.szpejsoft.zegarek.domain.model.Level
import io.reactivex.Observable

class LevelDao(private val clockDB: IClockDB) : ILevelDao {
    override val observeLevel: Observable<Level> = clockDB.levelObservable

    override fun saveLevel(level: Level) = clockDB.saveLevel(level)
}