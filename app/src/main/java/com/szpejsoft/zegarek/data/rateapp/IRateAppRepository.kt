package com.szpejsoft.zegarek.data.rateapp

import io.reactivex.Observable

interface IRateAppRepository {
    val showRateAppDialogObservable: Observable<Unit>

    fun setShowRateAppDialog(show: Boolean)
    fun decreaseNumberOfStartsToNextRateAppDialog()
}