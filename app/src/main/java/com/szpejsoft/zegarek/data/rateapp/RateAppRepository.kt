package com.szpejsoft.zegarek.data.rateapp

import io.reactivex.Observable
import io.reactivex.functions.BiFunction

class RateAppRepository(private val rateAppDao: IRateAppDao) : IRateAppRepository {
    override val showRateAppDialogObservable: Observable<Unit>
        get() {
            val showDialogObservable = rateAppDao.showRateAppDialogObservable
            val numberOfStartsObservable = rateAppDao.numberOfStartsToShowRateAppDialog
            return Observable.combineLatest(showDialogObservable, numberOfStartsObservable, BiFunction<Boolean, Int, Pair<Boolean, Int>> { showDialog, number -> showDialog to number })
                    .filter { pair -> pair.first && pair.second == 0 }
                    .map { Unit }
        }

    override fun setShowRateAppDialog(show: Boolean) {
        rateAppDao.saveShowRateAppDialog(show)
    }

    override fun decreaseNumberOfStartsToNextRateAppDialog() {
        rateAppDao.decreaseNumberOfStartsToNextRateAppDialog()
    }
}