package com.szpejsoft.zegarek.data.exercises

import com.szpejsoft.zegarek.domain.model.ExercisesNumberSettings
import io.reactivex.Observable
import io.reactivex.Single

interface IExercisesNumberRepository {
    val observeExercises: Observable<ExercisesNumberSettings>
    fun getExercisesNumberSettings(): Single<ExercisesNumberSettings>
    fun saveExercisesNumber(exercisesNumber: Int)

}