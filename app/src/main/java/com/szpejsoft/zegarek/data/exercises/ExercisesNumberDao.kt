package com.szpejsoft.zegarek.data.exercises

import com.szpejsoft.zegarek.data.common.IClockDB
import com.szpejsoft.zegarek.domain.model.ExercisesNumberSettings
import io.reactivex.Observable

class ExercisesNumberDao(private val clockDB: IClockDB) : IExercisesNumberDao {
    override val observeExercisesNumber: Observable<ExercisesNumberSettings> = clockDB.exercisesNumberSettingsObservable

    override fun saveExercisesNumber(exercisesNumber: Int) = clockDB.saveExercisesNumber(exercisesNumber)
}