package com.szpejsoft.zegarek.data.numerals

import com.szpejsoft.zegarek.data.common.IClockDB
import com.szpejsoft.zegarek.domain.model.NumeralsType
import io.reactivex.Observable

class NumeralsDao(private val clockDB: IClockDB) : INumeralsDao {
    override val numeralsTypeObservable: Observable<NumeralsType> = clockDB.numeralsTypeObservable

    override fun saveNumeralsTypeObservable(type: NumeralsType) {
        clockDB.saveNumeralsType(type)
    }
}