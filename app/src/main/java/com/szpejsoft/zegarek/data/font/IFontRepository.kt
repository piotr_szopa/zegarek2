package com.szpejsoft.zegarek.data.font

import com.szpejsoft.zegarek.domain.model.Font
import io.reactivex.Observable

interface IFontRepository {
    val fontObservable: Observable<Font>
    fun saveFont(font: Font)
}