package com.szpejsoft.zegarek.data.level

import com.szpejsoft.zegarek.domain.model.Level
import io.reactivex.Observable

interface ILevelDao {
    val observeLevel: Observable<Level>
    fun saveLevel(level: Level)
}
