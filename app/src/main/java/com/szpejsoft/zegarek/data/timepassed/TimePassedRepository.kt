package com.szpejsoft.zegarek.data.timepassed

import com.szpejsoft.zegarek.domain.model.*
import com.szpejsoft.zegarek.domain.model.exercises.TimePassedExercise
import io.reactivex.Single

class TimePassedRepository : ITimePassedRepository {
    override fun getExercises(howMany: Int, level: Level): Single<List<TimePassedExercise>> {
        val exercises = mutableSetOf<TimePassedExercise>()
        while (exercises.size < howMany) {
            exercises.add(getExercise(level))
        }
        return Single.just(exercises.toList())
    }

    private fun getExercise(level: Level): TimePassedExercise {
        val hm1 = level.createInstant()
        var hm2: Instant
        do {
            hm2 = level.createInstant()
        } while (hm1 == hm2)
        val startTime = if (hm1 < hm2) hm1 else hm2
        val endTime = if (hm1 < hm2) hm2 else hm1

        val answers = mutableSetOf(endTime.howMuchTimePassedFrom(startTime))
        while (answers.size < 4) {
            answers.add(level.createPeriod())
        }
        return TimePassedExercise(startTime, endTime, answers.toList().shuffled())
    }

}