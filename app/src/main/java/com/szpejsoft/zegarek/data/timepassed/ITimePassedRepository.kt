package com.szpejsoft.zegarek.data.timepassed

import com.szpejsoft.zegarek.domain.model.Level
import com.szpejsoft.zegarek.domain.model.exercises.TimePassedExercise
import io.reactivex.Single

interface ITimePassedRepository {
    fun getExercises(howMany: Int, level: Level): Single<List<TimePassedExercise>>
}