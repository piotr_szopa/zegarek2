package com.szpejsoft.zegarek.data.level

import com.szpejsoft.zegarek.domain.model.Level
import io.reactivex.Observable

class LevelRepository(private val levelDao: ILevelDao) : ILevelRepository {

    override val observeLevel: Observable<Level> = levelDao.observeLevel

    override fun saveLevel(level: Level) = levelDao.saveLevel(level)
}