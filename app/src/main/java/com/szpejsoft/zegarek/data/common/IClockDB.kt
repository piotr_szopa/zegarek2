package com.szpejsoft.zegarek.data.common

import com.szpejsoft.zegarek.domain.model.ExercisesNumberSettings
import com.szpejsoft.zegarek.domain.model.Font
import com.szpejsoft.zegarek.domain.model.Level
import com.szpejsoft.zegarek.domain.model.NumeralsType
import io.reactivex.Observable

interface IClockDB {
    val levelObservable: Observable<Level>
    val musicStateObservable: Observable<Boolean>
    val soundStateObservable: Observable<Boolean>
    val fontObservable: Observable<Font>
    val numeralsTypeObservable: Observable<NumeralsType>
    val exercisesNumberSettingsObservable: Observable<ExercisesNumberSettings>
    val showRateAppDialogObservable: Observable<Boolean>
    val numberOfStartsToShowRateAppDialog: Observable<Int>

    fun saveLevel(level: Level)
    fun saveMusicState(musicOn: Boolean)
    fun saveSoundState(soundOn: Boolean)
    fun saveFont(font: Font)
    fun saveNumeralsType(numeralsType: NumeralsType)
    fun saveExercisesNumber(exercisesNumber: Int)
    fun setShowRateAppDialog(show: Boolean)
    fun decreaseNumberOfStartsToNextRateAppDialog()

}