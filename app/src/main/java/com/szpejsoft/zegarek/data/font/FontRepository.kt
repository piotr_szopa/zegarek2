package com.szpejsoft.zegarek.data.font

import com.szpejsoft.zegarek.domain.model.Font
import io.reactivex.Observable

class FontRepository(private val fontDao: IFontDao) : IFontRepository {
    override val fontObservable: Observable<Font> = fontDao.fontObservable

    override fun saveFont(font: Font) = fontDao.saveFont(font)

}