package com.szpejsoft.zegarek.data.whattimeitis

import com.szpejsoft.zegarek.domain.model.Level
import com.szpejsoft.zegarek.domain.model.exercises.WhatTimeItIsExercise
import io.reactivex.Single

interface IWhatTimeItIsRepository {
    fun getExercises(howMany: Int, level: Level): Single<List<WhatTimeItIsExercise>>
}