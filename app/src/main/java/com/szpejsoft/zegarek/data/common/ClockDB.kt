package com.szpejsoft.zegarek.data.common

import android.content.Context
import android.content.SharedPreferences
import com.jakewharton.rxrelay2.BehaviorRelay
import com.szpejsoft.zegarek.domain.model.ExercisesNumberSettings
import com.szpejsoft.zegarek.domain.model.Font
import com.szpejsoft.zegarek.domain.model.Level
import com.szpejsoft.zegarek.domain.model.NumeralsType

class ClockDB(context: Context) : IClockDB {
    override val levelObservable: BehaviorRelay<Level> = BehaviorRelay.create()
    override val musicStateObservable: BehaviorRelay<Boolean> = BehaviorRelay.create()
    override val soundStateObservable: BehaviorRelay<Boolean> = BehaviorRelay.create()
    override val fontObservable: BehaviorRelay<Font> = BehaviorRelay.create()
    override val numeralsTypeObservable: BehaviorRelay<NumeralsType> = BehaviorRelay.create()
    override val exercisesNumberSettingsObservable: BehaviorRelay<ExercisesNumberSettings> = BehaviorRelay.create()
    override val showRateAppDialogObservable: BehaviorRelay<Boolean> = BehaviorRelay.create()
    override val numberOfStartsToShowRateAppDialog: BehaviorRelay<Int> = BehaviorRelay.create()

    private val prefs: SharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
    private val changeListener: SharedPreferences.OnSharedPreferenceChangeListener

    init {
        changeListener = SharedPreferences.OnSharedPreferenceChangeListener { _, key ->
            when (key) {
                LEVEL_KEY -> levelObservable.accept(getLevel())
                MUSIC_STATE_KEY -> musicStateObservable.accept(getMusicOn())
                SOUND_STATE_ON_KEY -> soundStateObservable.accept(getSoundOn())
                FONT_KEY -> fontObservable.accept(getFont())
                NUMERALS_TYPE_KEY -> numeralsTypeObservable.accept(getNumeralsType())
                EXERCISES_NUMBER_KEY -> exercisesNumberSettingsObservable.accept(getExercisesNumberSettings())
                SHOW_RATE_APP_KEY -> showRateAppDialogObservable.accept(getShowRateAppDialog())
                APP_STARTS_TO_NEXT_RATE_APP_DIALOG_KEY -> numberOfStartsToShowRateAppDialog.accept(getNumberOfStartsToNextRateAppDialog())
            }
        }
        prefs.registerOnSharedPreferenceChangeListener(changeListener)
        levelObservable.accept(getLevel())
        musicStateObservable.accept(getMusicOn())
        soundStateObservable.accept(getSoundOn())
        fontObservable.accept(getFont())
        numeralsTypeObservable.accept(getNumeralsType())
        exercisesNumberSettingsObservable.accept(getExercisesNumberSettings())
        showRateAppDialogObservable.accept(getShowRateAppDialog())
        numberOfStartsToShowRateAppDialog.accept(getNumberOfStartsToNextRateAppDialog())
    }


    override fun saveLevel(level: Level) {
        prefs.edit().putInt(LEVEL_KEY, level.ordinal).apply()
    }

    override fun saveMusicState(musicOn: Boolean) {
        prefs.edit().putBoolean(MUSIC_STATE_KEY, musicOn).apply()
    }

    override fun saveSoundState(soundOn: Boolean) {
        prefs.edit().putBoolean(SOUND_STATE_ON_KEY, soundOn).apply()
    }

    override fun saveFont(font: Font) {
        prefs.edit().putInt(FONT_KEY, font.ordinal).apply()
    }

    override fun saveNumeralsType(numeralsType: NumeralsType) {
        prefs.edit().putInt(NUMERALS_TYPE_KEY, numeralsType.ordinal).apply()
    }

    override fun saveExercisesNumber(exercisesNumber: Int) {
        prefs.edit().putInt(EXERCISES_NUMBER_KEY, exercisesNumber).apply()
    }

    override fun setShowRateAppDialog(show: Boolean) {
        prefs.edit().putBoolean(SHOW_RATE_APP_KEY, show).apply()
    }

    override fun decreaseNumberOfStartsToNextRateAppDialog() {
        val current = prefs.getInt(APP_STARTS_TO_NEXT_RATE_APP_DIALOG_KEY, SHOW_RATE_APP_PERIOD)
        val new = ((current - 1) + SHOW_RATE_APP_PERIOD) % SHOW_RATE_APP_PERIOD
        prefs.edit().putInt(APP_STARTS_TO_NEXT_RATE_APP_DIALOG_KEY, new).apply()
    }

    private fun getLevel() = Level.values()[prefs.getInt(LEVEL_KEY, LEVEL_DEFAULT_VALUE)]
    private fun getMusicOn() = prefs.getBoolean(MUSIC_STATE_KEY, MUSIC_TURNED_ON_DEFAULT_VALUE)
    private fun getSoundOn() = prefs.getBoolean(SOUND_STATE_ON_KEY, SOUND_TURNED_ON_DEFAULT_VALUE)
    private fun getFont() = Font.values()[prefs.getInt(FONT_KEY, FONT_DEFAULT_VALUE)]
    private fun getNumeralsType() = NumeralsType.values()[prefs.getInt(NUMERALS_TYPE_KEY, NUMERALS_TYPE_DEFAULT_VALUE)]
    private fun getExercisesNumberSettings() = ExercisesNumberSettings(prefs.getInt(EXERCISES_NUMBER_KEY, EXERCISES_NUMBER_DEFAULT_VALUE), 1, 10)
    private fun getShowRateAppDialog(): Boolean = prefs.getBoolean(SHOW_RATE_APP_KEY, SHOW_RATE_APP_DEFAULT_VALUE)
    private fun getNumberOfStartsToNextRateAppDialog(): Int = prefs.getInt(APP_STARTS_TO_NEXT_RATE_APP_DIALOG_KEY, SHOW_RATE_APP_PERIOD)

    companion object {
        private const val PREFS_NAME = "clock_database"
        private const val LEVEL_KEY = "level"
        private const val LEVEL_DEFAULT_VALUE = 0
        private const val MUSIC_STATE_KEY = "music"
        private const val MUSIC_TURNED_ON_DEFAULT_VALUE = true
        private const val SOUND_STATE_ON_KEY = "sound"
        private const val SOUND_TURNED_ON_DEFAULT_VALUE = true
        private const val FONT_KEY = "font"
        private const val FONT_DEFAULT_VALUE = 0
        private const val NUMERALS_TYPE_KEY = "numerals"
        private const val NUMERALS_TYPE_DEFAULT_VALUE = 0
        private const val EXERCISES_NUMBER_KEY = "exercises"
        private const val EXERCISES_NUMBER_DEFAULT_VALUE = 5
        private const val SHOW_RATE_APP_KEY = "dont_show_rate_app"
        private const val SHOW_RATE_APP_DEFAULT_VALUE = true
        private const val APP_STARTS_TO_NEXT_RATE_APP_DIALOG_KEY = "app_starts_to_next_rate_dialog"
        private const val SHOW_RATE_APP_PERIOD = 5
    }
}