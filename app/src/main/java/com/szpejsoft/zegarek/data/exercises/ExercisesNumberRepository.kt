package com.szpejsoft.zegarek.data.exercises

import com.szpejsoft.zegarek.domain.model.ExercisesNumberSettings
import io.reactivex.Observable
import io.reactivex.Single

class ExercisesNumberRepository(private val exercisesNumberDao: IExercisesNumberDao) : IExercisesNumberRepository {
    override val observeExercises: Observable<ExercisesNumberSettings> =
            exercisesNumberDao.observeExercisesNumber

    override fun getExercisesNumberSettings(): Single<ExercisesNumberSettings> =
            exercisesNumberDao.observeExercisesNumber.firstOrError()

    override fun saveExercisesNumber(exercisesNumber: Int) {
        exercisesNumberDao.saveExercisesNumber(exercisesNumber)
    }
}