package com.szpejsoft.zegarek.data.whattimeitis

import com.szpejsoft.zegarek.domain.model.Instant
import com.szpejsoft.zegarek.domain.model.Level
import com.szpejsoft.zegarek.domain.model.createInstant
import com.szpejsoft.zegarek.domain.model.exercises.WhatTimeItIsExercise
import io.reactivex.Single

class WhatTimeItIsRepository : IWhatTimeItIsRepository {

    override fun getExercises(howMany: Int, level: Level): Single<List<WhatTimeItIsExercise>> {
        val exercises = mutableSetOf<WhatTimeItIsExercise>()
        while (exercises.size < howMany) {
            exercises.add(getExercise(level))
        }
        return Single.just(exercises.toList())
    }

    private fun getExercise(level: Level): WhatTimeItIsExercise {
        val question = level.createInstant()
        val answers: List<Instant> = getAnswers(question, level)
        return WhatTimeItIsExercise(question, answers)
    }

    private fun getAnswers(question: Instant, level: Level): List<Instant> {
        val answers = mutableSetOf(question)
        do {
            answers.add(level.createInstant())
        } while (answers.size < 4)
        return answers.shuffled()
    }


}