package com.szpejsoft.zegarek.data.whattimewillbe

import com.szpejsoft.zegarek.domain.model.Level
import com.szpejsoft.zegarek.domain.model.exercises.WhatTimeWillBeExercise
import io.reactivex.Single

interface IWhatTimeWillBeRepository {
    fun getExercises(howMany: Int, level: Level): Single<List<WhatTimeWillBeExercise>>
}