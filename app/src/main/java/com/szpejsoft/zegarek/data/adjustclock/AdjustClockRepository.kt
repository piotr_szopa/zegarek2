package com.szpejsoft.zegarek.data.adjustclock

import com.szpejsoft.zegarek.domain.model.exercises.AdjustClockExercise
import com.szpejsoft.zegarek.domain.model.Instant
import com.szpejsoft.zegarek.domain.model.Level
import com.szpejsoft.zegarek.domain.model.createInstant
import io.reactivex.Single

class AdjustClockRepository : IAdjustClockRepository {
    override fun getExercises(howMany: Int, level: Level): Single<List<AdjustClockExercise>> {
        val exercises = mutableSetOf<AdjustClockExercise>()
        while (exercises.size < howMany) {
            exercises.add(getExercise(level))
        }
        return Single.just(exercises.toList())
    }

    private fun getExercise(level: Level): AdjustClockExercise {
        val question = level.createInstant()
        var initialTime: Instant
        do {
            initialTime = level.createInstant()
        } while (question == initialTime)
        return AdjustClockExercise(question, initialTime)
    }

}