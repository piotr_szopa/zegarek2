package com.szpejsoft.zegarek.data.exercises

import com.szpejsoft.zegarek.domain.model.ExercisesNumberSettings
import io.reactivex.Observable

interface IExercisesNumberDao {
    val observeExercisesNumber: Observable<ExercisesNumberSettings>

    fun saveExercisesNumber(exercisesNumber: Int)
}