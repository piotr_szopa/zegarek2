package com.szpejsoft.zegarek.ui.settings


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.szpejsoft.zegarek.R
import com.szpejsoft.zegarek.domain.model.ExercisesNumberSettings
import com.szpejsoft.zegarek.domain.model.Font
import com.szpejsoft.zegarek.ui.base.BaseFragment
import kotlinx.android.synthetic.main.settings_fragment.*
import org.koin.android.viewmodel.ext.android.viewModel

class SettingsFragment : BaseFragment() {

    private val settingsViewModel: ISettingsViewModel by viewModel<SettingsViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.settings_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(false)
    }


    override fun bindViewModel() {
        observeViewModel()
        setupButtonListeners()
    }

    private fun observeViewModel() {
        settingsViewModel.musicOnLiveData.observe(this, Observer { setupMusicSwitch(it) })
        settingsViewModel.soundOnLiveData.observe(this, Observer { setupSoundSwitch(it) })
    }

    private fun setupSoundSwitch(soundOn: Boolean) {
        if (settingsSoundSwitch.isChecked != soundOn) {
            settingsSoundSwitch.isChecked = soundOn
        }
    }

    private fun setupMusicSwitch(musicOn: Boolean) {
        if (settingsMusicSwitch.isChecked != musicOn) {
            settingsMusicSwitch.isChecked = musicOn
        }
    }

    private fun setupButtonListeners() {
        settingsAboutButton.setOnClickListener { navigateToAboutScreen() }
        settingsLevelButton.setOnClickListener { showSelectLevelDialog() }
        settingsFontButton.setOnClickListener { showSelectFontDialog() }
        settingsNumeralsButton.setOnClickListener { showSelectNumeralsDialog() }
        settingsExercisesButton.setOnClickListener { showExercisesNumberDialog() }
        settingsMusicSwitch.setOnCheckedChangeListener { _, musicOn -> settingsViewModel.onMusicSwitched(musicOn) }
        settingsSoundSwitch.setOnCheckedChangeListener { _, soundOn -> settingsViewModel.onSoundSwitched(soundOn) }
    }

    private fun navigateToAboutScreen() {
        Navigation.findNavController(settingsRootView).navigate(R.id.settings_to_aboutFragment)
    }

    private fun showExercisesNumberDialog() {
        activity?.let {
            val settings = settingsViewModel.exercisesNumberLiveData.value
                    ?: ExercisesNumberSettings(5)
            val font: Font = settingsViewModel.fontLiveData.value ?: Font.ROBOTO
            dialogFactory.createChooseExercisesNumberDialog(it, settings, font) { exercisesNumber ->
                settingsViewModel.onExercisesNumberChosen(exercisesNumber)
            }.show()
        }
    }

    private fun showSelectLevelDialog() {
        activity?.let { act ->
            val selectedLevel = settingsViewModel.levelLiveData.value ?: return@let
            dialogFactory.createSelectLevelDialog(act, selectedLevel) { level ->
                settingsViewModel.onLevelChosen(level)
            }.show()
        }
    }

    private fun showSelectFontDialog() {
        activity?.let { act ->
            val selectedFont = settingsViewModel.fontLiveData.value ?: return@let
            dialogFactory.createSelectFontDialog(act, selectedFont) { font ->
                settingsViewModel.onFontChosen(font, act)
            }.show()
        }
    }

    private fun showSelectNumeralsDialog() {
        activity?.let { act ->
            val selectedNumerals = settingsViewModel.numeralsLiveData.value ?: return@let
            dialogFactory.createSelectNumeralsDialog(act, selectedNumerals) { numerals ->
                settingsViewModel.onNumeralsChosen(numerals)
            }.show()
        }
    }
}
