package com.szpejsoft.zegarek.ui.base

import android.app.Dialog
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.szpejsoft.zegarek.domain.model.AnswerEvaluation
import com.szpejsoft.zegarek.ui.dialogs.IDialogFactory
import com.szpejsoft.zegarek.ui.main.MainActivity
import org.koin.android.ext.android.inject
import java.util.*

abstract class BaseFragment : Fragment() {

    protected val dialogFactory: IDialogFactory by inject()

    private val timer = Timer()

    private var visibleDialog: Dialog? = null

    private var clockActivity: MainActivity? = null
        get() = activity as MainActivity?

    protected abstract fun bindViewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        bindViewModel()
    }

    protected fun setVisibleDialog(dialog: Dialog?, autoDismiss: Boolean) {
        if (visibleDialog != null) return
        visibleDialog = dialog
        dialog?.show()
        if (autoDismiss) {
            timer.schedule(object : TimerTask() {
                override fun run() {
                    visibleDialog?.dismiss()
                    visibleDialog = null
                }
            }, 2000)
        }
    }

    protected fun playEvaluationSound(evaluation: AnswerEvaluation) =
            when (evaluation) {
                AnswerEvaluation.SUCCESS -> clockActivity?.soundService?.playSuccess()
                AnswerEvaluation.FAILURE -> clockActivity?.soundService?.playFailure()
            }

    protected fun playExerciseFinishedSound() =
            clockActivity?.soundService?.playLevelEnd()
}