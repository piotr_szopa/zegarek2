package com.szpejsoft.zegarek.ui.splash

import androidx.lifecycle.LiveData

interface ISplashFragmentViewModel {
    val showAnimationLiveData: LiveData<Unit>
    fun init()
}