package com.szpejsoft.zegarek.ui.help

import android.text.Spanned
import androidx.lifecycle.LiveData
import com.szpejsoft.zegarek.domain.model.exercises.ChosenExercise

interface IHelpViewModel {
    fun setChosenExercise(exercise: ChosenExercise)
    val helpData: LiveData<Spanned>
}