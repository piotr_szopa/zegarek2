package com.szpejsoft.zegarek.ui.whattimeitis

import android.view.View
import androidx.lifecycle.LiveData
import com.szpejsoft.zegarek.domain.model.AnswerEvaluation
import com.szpejsoft.zegarek.domain.model.ExerciseMode
import com.szpejsoft.zegarek.domain.model.Font
import com.szpejsoft.zegarek.domain.model.NumeralsType
import com.szpejsoft.zegarek.domain.model.exercises.WhatTimeItIsExercise

interface IWhatTimeItIsViewModel {

    val whatTimeItIsExerciseData: LiveData<WhatTimeItIsExercise>
    val numeralsTypeData: LiveData<NumeralsType>
    val adjustmentPrecisionData: LiveData<Int>
    val shownDigitsStepData: LiveData<Int>

    var mode: ExerciseMode

    //TODO these LiveDatas will be probably moved to BaseViewModel
    val adjustmentModeData: LiveData<Boolean>
    val evaluationData: LiveData<AnswerEvaluation>
    val exercisesInSetData: LiveData<Int>
    val exercisesLeftData: LiveData<Int>
    val finishData: LiveData<Int>
    val fontData: LiveData<Font>

    fun onAnswerClicked(answerOrdinal: Int)
    fun onEvaluationDialogDismissed()
    //TODO change view to fragment
    fun onEndDialogDismissed(view: View)
}