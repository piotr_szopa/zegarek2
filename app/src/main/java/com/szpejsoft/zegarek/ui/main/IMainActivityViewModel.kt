package com.szpejsoft.zegarek.ui.main

import com.szpejsoft.zegarek.domain.model.Font

interface IMainActivityViewModel {

    fun getFont(): Font
}