package com.szpejsoft.zegarek.ui.about

import android.text.Spanned
import androidx.lifecycle.LiveData

interface IAboutViewModel {
    val musicDescriptionData: LiveData<Spanned>
    val soundDescriptionData: LiveData<Spanned>
    val fontDescriptionData: LiveData<Spanned>
}