package com.szpejsoft.zegarek.ui.music

import android.app.Service
import android.content.Context
import android.content.Intent
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.MediaPlayer
import android.media.SoundPool
import android.os.Binder
import android.os.IBinder
import com.szpejsoft.zegarek.R
import java.util.concurrent.atomic.AtomicBoolean

class SoundService : Service() {
    private var musicPlayer: MediaPlayer? = null
    private var soundPlayer: SoundPool? = null
    private lateinit var audioManager: AudioManager

    private val iBinder = SoundServiceBinder()
    private var successStreamId: Int = 0
    private var failureStreamId: Int = 0
    private var endLevelStreamId: Int = 0
    private var currentStreamId: Int = 0
    private var maxVolume: Int = 1
    private var isPlayingMusic: AtomicBoolean = AtomicBoolean(false)


    inner class SoundServiceBinder : Binder() {
        val soundService: SoundService
            get() = this@SoundService
    }

    override fun onBind(intent: Intent): IBinder? {
        audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC)
        val audioAttributes = AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                .setUsage(AudioAttributes.USAGE_ASSISTANCE_SONIFICATION)
                .build()

        soundPlayer = SoundPool.Builder()
                .setAudioAttributes(audioAttributes)
                .setMaxStreams(3)
                .build()
        successStreamId = soundPlayer?.load(this, R.raw.success, 1) ?: 0
        failureStreamId = soundPlayer?.load(this, R.raw.failure, 1) ?: 0
        endLevelStreamId = soundPlayer?.load(this, R.raw.game_win, 1) ?: 0
        return iBinder
    }

    override fun onUnbind(intent: Intent): Boolean {
        musicPlayer?.release()
        musicPlayer = null
        isPlayingMusic.set(false)

        soundPlayer?.release()
        soundPlayer = null
        return false
    }

    fun playMusic() {
        if (isPlayingMusic.getAndSet(true)) return
        musicPlayer = MediaPlayer.create(applicationContext, R.raw.splash)
                .also {
                    it.isLooping = true
                    it.start()
                }
    }

    fun stopPlayingMusic() {
        musicPlayer?.release()
        musicPlayer = null
        isPlayingMusic.set(false)
    }

    fun playSuccess() = play(successStreamId)

    fun playFailure() = play(failureStreamId)

    fun playLevelEnd() = play(endLevelStreamId)

    private fun play(streamId: Int) {
        val currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC).toFloat() / maxVolume
        soundPlayer?.let {
            it.stop(currentStreamId)
            currentStreamId = it.play(streamId, currentVolume, currentVolume, 1, 0, 1.0f)
        }
    }
}
