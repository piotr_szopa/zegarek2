package com.szpejsoft.zegarek.ui.whattimeitis

import android.app.Application
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.navigation.Navigation
import com.szpejsoft.zegarek.base.SchedulersFacade
import com.szpejsoft.zegarek.base.SingleLiveEvent
import com.szpejsoft.zegarek.domain.model.*
import com.szpejsoft.zegarek.domain.model.exercises.WhatTimeItIsExercise
import com.szpejsoft.zegarek.domain.usecase.whattimeitis.IWhatTimeItIsUseCase
import com.szpejsoft.zegarek.ui.base.BaseViewModel
import com.szpejsoft.zegarek.ui.base.ExerciseStrategy
import com.szpejsoft.zegarek.ui.base.LearnStrategy
import com.szpejsoft.zegarek.ui.base.TestStrategy

class WhatTimeItIsViewModel(application: Application,
                            schedulersFacade: SchedulersFacade,
                            private val whatTimeItIsUseCase: IWhatTimeItIsUseCase
) : BaseViewModel(application, schedulersFacade), IWhatTimeItIsViewModel {
    override val adjustmentModeData = MutableLiveData<Boolean>()
    override val exercisesInSetData = MutableLiveData<Int>()
    override val exercisesLeftData = MutableLiveData<Int>()
    override val whatTimeItIsExerciseData = MutableLiveData<WhatTimeItIsExercise>()
    override val evaluationData = SingleLiveEvent<AnswerEvaluation>()
    override val finishData = SingleLiveEvent<Int>()
    override val fontData = MutableLiveData<Font>()
    override val numeralsTypeData = MutableLiveData<NumeralsType>()
    override val adjustmentPrecisionData = MutableLiveData<Int>()
    override val shownDigitsStepData = MutableLiveData<Int>()

    private val exerciseList = mutableListOf<WhatTimeItIsExercise>()
    private var strategy: ExerciseStrategy<WhatTimeItIsExercise>? = null
    private var currentItIsExercise: WhatTimeItIsExercise? = null
        set(value) {
            field = value
            whatTimeItIsExerciseData.value = value
        }

    override var mode: ExerciseMode = ExerciseMode.LEARN
        set(value) {
            field = value
            strategy = when (value) {
                ExerciseMode.LEARN -> LearnStrategy()
                ExerciseMode.TEST -> TestStrategy<WhatTimeItIsExercise>()
                        .apply { this.exercises = exerciseList.size }
            }
        }

    init {
        whatTimeItIsUseCase.getExercises()
                .subscribeBy { exercises -> setupExercises(exercises) }
        whatTimeItIsUseCase.getClockSettings()
                .subscribeBy { clockSettings -> setupClock(clockSettings) }
    }

    override fun onAnswerClicked(answerOrdinal: Int) {
        val safeStrategy = strategy ?: throw IllegalStateException("strategy must be set")
        if (isProperAnswer(answerOrdinal)) {
            safeStrategy.handleProperAnswer(exerciseList, currentItIsExercise!!, finishData, evaluationData)
        } else {
            safeStrategy.handleWrongAnswer(exerciseList, currentItIsExercise!!, finishData, evaluationData)
        }
        exercisesLeftData.value = exerciseList.size
    }

    override fun onEvaluationDialogDismissed() {
        currentItIsExercise = getRandomExercise()
    }

    override fun onEndDialogDismissed(view: View) {
        Navigation.findNavController(view).navigateUp()
    }

    private fun setupClock(clockSettings: ClockSettings) {
        clockSettings.apply {
            fontData.value = font
            numeralsTypeData.value = numeralsType
            shownDigitsStepData.value = shownDigits.step
            adjustmentPrecisionData.value = adjustmentPrecision.step
            adjustmentModeData.value = clockSettings.isAdjustmentMode
        }
    }

    private fun getRandomExercise(): WhatTimeItIsExercise = exerciseList[nextInt(exerciseList.size)]

    private fun setupExercises(itIsExercises: List<WhatTimeItIsExercise>) {
        this.exerciseList.apply {
            clear()
            addAll(itIsExercises)
        }
        exercisesInSetData.value = exerciseList.size
        exercisesLeftData.value = exerciseList.size
        currentItIsExercise = getRandomExercise()
        val safeStrategy = strategy ?: return
        if (safeStrategy is TestStrategy) {
            safeStrategy.exercises = exerciseList.size
        }
    }

    private fun isProperAnswer(answerOrdinal: Int): Boolean {
        val safeExercise = currentItIsExercise ?: return false
        return safeExercise.answers[answerOrdinal] == safeExercise.question
    }


}