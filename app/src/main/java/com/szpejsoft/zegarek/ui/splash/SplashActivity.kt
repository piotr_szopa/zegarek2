package com.szpejsoft.zegarek.ui.splash

import android.content.pm.ActivityInfo
import android.os.Bundle
import com.szpejsoft.zegarek.R
import com.szpejsoft.zegarek.ui.music.MusicActivity

class SplashActivity : MusicActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        requestedOrientation = if (isPhone()) ActivityInfo.SCREEN_ORIENTATION_PORTRAIT else ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_activity)
        val fragment: SplashFragment = supportFragmentManager
                .findFragmentByTag(TAG) as? SplashFragment
                ?: SplashFragment()
        supportFragmentManager.beginTransaction()
                .replace(R.id.splashContainer, fragment, TAG)
                .commitNow()
    }

    companion object {
        private const val TAG = "splash"
    }

}