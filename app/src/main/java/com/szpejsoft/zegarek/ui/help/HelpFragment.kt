package com.szpejsoft.zegarek.ui.help

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.szpejsoft.zegarek.R
import com.szpejsoft.zegarek.ui.base.BaseFragment
import kotlinx.android.synthetic.main.help_fragment.*
import org.koin.android.viewmodel.ext.android.viewModel

class HelpFragment : BaseFragment() {
    private val helpViewModel: IHelpViewModel by viewModel<HelpViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.help_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(false)
    }

    override fun bindViewModel() {
        helpViewModel.setChosenExercise(HelpFragmentArgs.fromBundle(arguments!!).exercise)
        helpViewModel.helpData.observe(this, Observer { help -> helpContent.text = help })
    }
}