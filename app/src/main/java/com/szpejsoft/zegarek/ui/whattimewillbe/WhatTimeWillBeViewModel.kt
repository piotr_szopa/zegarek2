package com.szpejsoft.zegarek.ui.whattimewillbe

import android.app.Application
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.navigation.Navigation
import com.szpejsoft.zegarek.base.SchedulersFacade
import com.szpejsoft.zegarek.base.SingleLiveEvent
import com.szpejsoft.zegarek.domain.model.AnswerEvaluation
import com.szpejsoft.zegarek.domain.model.ExerciseMode
import com.szpejsoft.zegarek.domain.model.add
import com.szpejsoft.zegarek.domain.model.exercises.WhatTimeWillBeExercise
import com.szpejsoft.zegarek.domain.usecase.whattimewillbe.IWhatTimeWillBeUseCase
import com.szpejsoft.zegarek.ui.base.BaseViewModel
import com.szpejsoft.zegarek.ui.base.ExerciseStrategy
import com.szpejsoft.zegarek.ui.base.LearnStrategy
import com.szpejsoft.zegarek.ui.base.TestStrategy

class WhatTimeWillBeViewModel(application: Application,
                              schedulersFacade: SchedulersFacade,
                              private val whatTimeWillBeUseCase: IWhatTimeWillBeUseCase
) : BaseViewModel(application, schedulersFacade), IWhatTimeWillBeViewModel {
    override val exercisesInSetData = MutableLiveData<Int>()
    override val exercisesLeftData = MutableLiveData<Int>()
    override val exerciseData = MutableLiveData<WhatTimeWillBeExercise>()
    override val evaluationData = SingleLiveEvent<AnswerEvaluation>()
    override val finishData = SingleLiveEvent<Int>()

    override var mode: ExerciseMode = ExerciseMode.LEARN
        set(value) {
            field = value
            strategy = when (value) {
                ExerciseMode.LEARN -> LearnStrategy()
                ExerciseMode.TEST -> TestStrategy<WhatTimeWillBeExercise>()
                        .apply { this.exercises = exerciseList.size }
            }
        }

    private var currentExercise: WhatTimeWillBeExercise? = null
        set(value) {
            field = value
            exerciseData.value = value
        }

    private var strategy: ExerciseStrategy<WhatTimeWillBeExercise>? = null
    private var exerciseList = mutableListOf<WhatTimeWillBeExercise>()

    init {
        whatTimeWillBeUseCase.getExercises().subscribeBy { exercises -> setupExercises(exercises) }
    }

    override fun onAnswerClicked(answerOrdinal: Int) {
        val safeStrategy = strategy ?: throw IllegalStateException("strategy must be set")
        if (isProperAnswer(answerOrdinal)) {
            safeStrategy.handleProperAnswer(exerciseList, currentExercise!!, finishData, evaluationData)
        } else {
            safeStrategy.handleWrongAnswer(exerciseList, currentExercise!!, finishData, evaluationData)
        }
        exercisesLeftData.value = exerciseList.size
    }

    private fun setupExercises(exercises: List<WhatTimeWillBeExercise>) {
        exerciseList.apply {
            clear()
            addAll(exercises)
        }
        exercisesInSetData.postValue(exerciseList.size)
        exercisesLeftData.postValue(exerciseList.size)
        currentExercise = getRandomExercise()
        val safeStrategy = strategy ?: return
        if (safeStrategy is TestStrategy) {
            safeStrategy.exercises = exerciseList.size
        }
    }

    override fun onEvaluationDialogDismissed() {
        currentExercise = getRandomExercise()
    }

    override fun onEndDialogDismissed(view: View) {
        Navigation.findNavController(view).navigateUp()
    }

    private fun getRandomExercise(): WhatTimeWillBeExercise = exerciseList[nextInt(exerciseList.size)]

    private fun isProperAnswer(answerOrdinal: Int): Boolean {
        val current = currentExercise ?: return false
        val answer = current.answers[answerOrdinal]
        val properAnswer = current.startTime.add(current.difference)
        return answer == properAnswer
    }

}