package com.szpejsoft.zegarek.ui.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.szpejsoft.zegarek.R
import com.szpejsoft.zegarek.domain.model.Font
import com.szpejsoft.zegarek.domain.model.Instant
import com.szpejsoft.zegarek.domain.model.NumeralsType
import com.szpejsoft.zegarek.domain.model.getFontResId

/**
 * Created by piotr on 27.11.16.
 */
class ClockView
@JvmOverloads
constructor(context: Context,
            attrs: AttributeSet? = null,
            defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {
    var font: Font = Font.ROBOTO
        set(value) {
            field = value
            invalidate()
        }

    private var hour: Int = 0
        set(value) {
            field = ((value % 24) + 24) % 24
            invalidate()
        }

    private var minute: Int = 0
        set(value) {
            when {
                value < 0 -> {
                    hour -= 1
                    field = ((value % 60) + 60) % 60
                }
                value in 0..59 -> {
                    field = value
                }
                value >= 60 -> {
                    hour += 1
                    field = ((value % 60) + 60) % 60
                }
            }
            invalidate()
        }


    //TODO create different paints for hands and numerals paint
    private val paint: Paint = Paint(Paint.SUBPIXEL_TEXT_FLAG)
    private val textRect: Rect
    var numeralsType = NumeralsType.ARABIC_NUMERALS
        set(value) {
            field = value
            invalidate()
        }

    var skippedDrawnDigits: Int = 1
        set(value) {
            field = value
            invalidate()
        }

    private var adjustmentMode: Boolean = false
    private var adjustedHand = AdjustedHand.NONE
    private var previousAngle = INVALID_ANGLE
    var adjustmentPrecision: Int = 1


    private val adjustmentListener = OnTouchListener { v, event ->
        val touchCoordinates = floatArrayOf(event.x, event.y)
        val currentAngle = getAngle(touchCoordinates)
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                chooseAdjustedHand(touchCoordinates, v.width, v.height)
                setTimeFromTouch(currentAngle)
                previousAngle = currentAngle
            }
            MotionEvent.ACTION_UP -> {
                setTimeFromTouch(currentAngle)
                previousAngle = INVALID_ANGLE
                adjustedHand = AdjustedHand.NONE
            }
            MotionEvent.ACTION_MOVE -> {
                setTimeFromTouch(currentAngle)
                previousAngle = currentAngle
            }
        }
        true
    }

    private val minuteHandEnd: FloatArray
        get() {
            val angle = ((180.0f - minute * 6) % 360)
            var end = polarToCartesian(MINUTE_HAND_LENGTH, angle)
            end = canvasToViewCoordinates(end)
            return end
        }

    private val hourHandEnd: FloatArray
        get() {
            val angle = (180.0f - (hour * 30) - minute * 0.5f) % 360
            var end = polarToCartesian(HOUR_HAND_LENGTH, angle)
            end = canvasToViewCoordinates(end)
            return end
        }


    private enum class AdjustedHand {
        NONE, HOUR, MINUTE
    }

    init {
        paint.style = Paint.Style.STROKE
        paint.color = Color.BLACK
        paint.isAntiAlias = true
        textRect = Rect()
        adjustmentMode = false
    }


    fun setTime(instant: Instant) {
        hour = instant.getHour()
        minute = instant.getMinute()
        invalidate()
    }

    fun getTime() = Instant(hour, minute)

    fun increaseHour() {
        hour += 1
    }

    fun decreaseHour() {
        hour -= 1
    }


    fun increaseMinute() {
        minute += adjustmentPrecision
    }

    fun decreaseMinute() {
        minute -= adjustmentPrecision
    }


    private fun setMinuteFromTouch(minute: Int) {
        var m = minute
        m = (m % 60 + 60) % 60
        val admissibleMinutes = IntArray(60 / adjustmentPrecision)
        for (i in admissibleMinutes.indices) {
            admissibleMinutes[i] = i * adjustmentPrecision
        }
        var minuteToSet = 0
        var minDifferenceSquared = Integer.MAX_VALUE
        for (admissibleMinute in admissibleMinutes) {
            val differenceSquared = (m - admissibleMinute) * (m - admissibleMinute)
            if (differenceSquared > minDifferenceSquared) {
                break
            }
            minDifferenceSquared = differenceSquared
            minuteToSet = admissibleMinute
        }

        this.minute = (minuteToSet % 60 + 60) % 60
        invalidate()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        val size = if (height < width) height else width

        canvas.save()
        canvas.translate((width / 2).toFloat(), (height / 2).toFloat())
        canvas.scale(size / MIN_SIZE, size / MIN_SIZE)
        drawClockFace(canvas)
        drawScale(canvas)
        drawDigits(canvas)
        drawHands(canvas)
        canvas.restore()
    }

    private fun drawHands(canvas: Canvas) {
        paint.strokeCap = Paint.Cap.ROUND
        paint.color = ContextCompat.getColor(context, R.color.minuteHand)
        paint.strokeWidth = 5f
        //hour hand
        var handEnd = polarToCartesian(HOUR_HAND_LENGTH, this.hour * 30 + minute * 0.5f)
        canvas.drawLine(0f, 0f, handEnd[0], handEnd[1], paint)
        if (adjustedHand == AdjustedHand.HOUR) {
            highLightEnd(canvas, handEnd)
        }
        //minute hand
        handEnd = polarToCartesian(MINUTE_HAND_LENGTH, minute * 6.0f)
        paint.color = ContextCompat.getColor(context, R.color.hourHand)
        paint.strokeWidth = 3f
        canvas.drawLine(0f, 0f, handEnd[0], handEnd[1], paint)
        if (adjustedHand == AdjustedHand.MINUTE) {
            highLightEnd(canvas, handEnd)
        }
        //circle in the origin
        paint.color = Color.BLACK
        paint.strokeWidth = 4f
        canvas.drawCircle(0f, 0f, 2f, paint)
    }

    private fun highLightEnd(canvas: Canvas, handEnd: FloatArray) {
        paint.alpha = 0x60
        canvas.drawCircle(handEnd[0], handEnd[1], 16.0f, paint)
        paint.alpha = 0x80
        canvas.drawCircle(handEnd[0], handEnd[1], 8.0f, paint)
        paint.alpha = 0xff
    }

    private fun drawClockFace(canvas: Canvas) {
        paint.strokeCap = Paint.Cap.BUTT
        paint.style = Paint.Style.FILL
        paint.color = Color.WHITE
        paint.strokeWidth = 3f
        canvas.drawCircle(0f, 0f, CLOCK_FACE_RADIUS, paint)

        paint.style = Paint.Style.STROKE
        paint.color = Color.BLACK
        canvas.drawCircle(0f, 0f, CLOCK_FACE_RADIUS, paint)
    }

    private fun drawScale(canvas: Canvas) {
        paint.strokeCap = Paint.Cap.BUTT
        paint.style = Paint.Style.STROKE
        paint.color = Color.BLACK
        for (i in 0..59) {
            val start = polarToCartesian((if (i % 5 == 0) 85 else 90).toFloat(), (i * 6).toFloat())
            val end = polarToCartesian(95f, (i * 6).toFloat())
            paint.strokeWidth = (if (i % 5 == 0) 3 else 2).toFloat()
            canvas.drawLine(start[0], start[1], end[0], end[1], paint)
        }
    }

    private fun drawDigits(canvas: Canvas) {
        draw12Digits(canvas)
//        if (shouldDraw24Digits()) {
//            drawDigits24(canvas)
//        }
    }

    private fun draw12Digits(canvas: Canvas) {
        paint.color = Color.BLACK
        paint.strokeWidth = 0.1f
        paint.textSize = 20f
        paint.style = Paint.Style.FILL_AND_STROKE
        paint.isFakeBoldText = true
        paint.isAntiAlias = true
        paint.typeface = ResourcesCompat.getFont(context, font.getFontResId())
        var i = 0
        while (i < 12) {
            val point = polarToCartesian(70f, i * 30.0f)
            drawTextCentered(canvas, paint, this.numeralsType.numerals[i], point[0], point[1])
            i += skippedDrawnDigits
        }
    }

//TODO decide whether use this
//    private var isShow24Hours = false
//    private fun shouldDraw24Digits() = this.numeralsType == NumeralsType.ARABIC_NUMERALS || isShow24Hours || DateFormat.is24HourFormat(context)


//    private fun drawDigits24(canvas: Canvas) {
//        var i = 1
//        while (i <= 12) {
//            val point = polarToCartesian(50f, i * 30.0f)
//            drawTextCentered(canvas, paint, "${(i + 12)}", point[0], point[1])
//            i += skippedDrawnDigits
//        }
//        postInvalidate()
//    }

    private fun drawTextCentered(c: Canvas, p: Paint, text: String, x: Float, y: Float) {
        p.textAlign = Paint.Align.CENTER
        p.getTextBounds(text, 0, text.length, textRect)
        c.drawText(text, x, y + Math.abs(textRect.height()) / 2, p)
    }

    fun setAdjustmentMode(adjustmentMode: Boolean) {
        if (this.adjustmentMode == adjustmentMode) {
            return
        }
        this.adjustmentMode = adjustmentMode
        if (this.adjustmentMode) {
            setOnTouchListener(adjustmentListener)
        } else {
            setOnTouchListener(null)
        }
    }

    private fun setHour(angle: Float) {
        val hour = (angle / 30).toInt()
        val minute = (angle - hour * 30).toInt() * 2
        this.hour = hour
        setMinuteFromTouch(minute)
    }

    private fun setTimeFromTouch(angle: Float) =
            when (adjustedHand) {
                ClockView.AdjustedHand.HOUR -> setHour(angle)
                ClockView.AdjustedHand.MINUTE -> setTime(angle)
                else -> {
                } //dunuthin
            }

    private fun setTime(angle: Float) {
        if (previousAngle < 0) {
            setMinuteFromTouch(angle)
        } else {
            if (previousAngle >= 360 - ANGLE_THRESHOLD && angle >= 0 && angle <= ANGLE_THRESHOLD) {
                hour += 1
            } else if (previousAngle < ANGLE_THRESHOLD && angle <= 360 && angle >= 360 - ANGLE_THRESHOLD) {
                hour -= 1
            }
            setMinuteFromTouch(angle)
        }
    }

    private fun setMinuteFromTouch(angle: Float) {
        val minute = (angle / 6).toInt()
        setMinuteFromTouch(minute)
    }

    private fun chooseAdjustedHand(pointer: FloatArray, w: Int, h: Int) {
        val threshold = (if (w < h) w else h) / 10.0f
        adjustedHand = when {
            getDistance(minuteHandEnd, pointer) < threshold -> AdjustedHand.MINUTE
            getDistance(hourHandEnd, pointer) < threshold -> AdjustedHand.HOUR
            else -> AdjustedHand.NONE
        }
    }

    private fun getDistance(point1: FloatArray, point2: FloatArray): Float {
        val dx = point1[0] - point2[0]
        val dy = point1[1] - point2[1]
        return Math.sqrt((dx * dx + dy * dy).toDouble()).toFloat()
    }

    //kąt 0 = na górze
    private fun getAngle(pointer: FloatArray): Float {
        val originX = (width / 2).toFloat()
        val originY = (height / 2).toFloat()
        return (180 - Math.atan2((pointer[0] - originX).toDouble(), (pointer[1] - originY).toDouble()) * 180 / Math.PI).toFloat()
    }

    private fun canvasToViewCoordinates(canvasCoordinates: FloatArray): FloatArray {
        val w = width
        val h = height
        val size = if (w < h) w else h
        val viewCoordinates = FloatArray(2)
        viewCoordinates[0] = canvasCoordinates[0] * size / MIN_SIZE + w / 2
        viewCoordinates[1] = -canvasCoordinates[1] * size / MIN_SIZE + h / 2
        return viewCoordinates
    }


    /**
     * przekręcone o 90 stopni w lewo żeby 12 miała kąt 0
     *
     * @param r
     * @param theta in degrees
     * @return
     */
    private fun polarToCartesian(r: Float, theta: Float): FloatArray {
        val thetaRad = (theta - 90) * Math.PI / 180
        return floatArrayOf((r * Math.cos(thetaRad)).toFloat(), (r * Math.sin(thetaRad)).toFloat())
    }

//    fun setShow24hours(show24hours: Boolean) {
//        this.isShow24Hours = show24hours
//    }

    companion object {
        private const val ANGLE_THRESHOLD = 20.0f
        private const val HOUR_HAND_LENGTH = 55.0f
        private const val MINUTE_HAND_LENGTH = 85.0f
        private const val CLOCK_FACE_RADIUS = 95.0f
        private const val INVALID_ANGLE = -1.0f
        private const val MIN_SIZE = 200.0f //centered square with edge of length MIN_SIZE is visible on ClockView
    }
}
