package com.szpejsoft.zegarek.ui.splash

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.szpejsoft.zegarek.base.SchedulersFacade
import com.szpejsoft.zegarek.ui.base.BaseViewModel
import io.reactivex.Completable
import java.util.concurrent.TimeUnit

class SplashFragmentViewModel(application: Application,
                              schedulersFacade: SchedulersFacade
) : BaseViewModel(application, schedulersFacade), ISplashFragmentViewModel {
    override val showAnimationLiveData: MutableLiveData<Unit> = MutableLiveData()

    override fun init() {
        Completable.complete()
                .delay(500, TimeUnit.MILLISECONDS)
                .subscribeBy { showAnimationLiveData.value = Unit }
    }
}