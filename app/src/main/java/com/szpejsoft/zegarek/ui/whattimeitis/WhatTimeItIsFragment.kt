package com.szpejsoft.zegarek.ui.whattimeitis

import android.os.Bundle
import android.view.*
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.szpejsoft.zegarek.R
import com.szpejsoft.zegarek.domain.model.*
import com.szpejsoft.zegarek.domain.model.exercises.ChosenExercise
import com.szpejsoft.zegarek.domain.model.exercises.WhatTimeItIsExercise
import com.szpejsoft.zegarek.ui.base.BaseFragment
import kotlinx.android.synthetic.main.exercises_counter.*
import kotlinx.android.synthetic.main.what_time_it_is_fragment.*
import org.koin.android.viewmodel.ext.android.viewModel

class WhatTimeItIsFragment : BaseFragment() {

    private val whatTimeItIsViewModel: IWhatTimeItIsViewModel by viewModel<WhatTimeItIsViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.what_time_it_is_fragment, container, false)
    }

    override fun bindViewModel() {
        whatTimeItIsViewModel.mode = WhatTimeItIsFragmentArgs.fromBundle(arguments!!).mode
        setupObservers()
        setupListeners()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.exercise_fragment_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item?.let {
            if (item.itemId == R.id.help) {
                val directions = WhatTimeItIsFragmentDirections.whatTimeItIsToHelp(ChosenExercise.WHAT_TIME_IT_IS)
                Navigation.findNavController(whatTimeItIsClock)
                        .navigate(directions)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupObservers() {
        whatTimeItIsViewModel.apply {
            adjustmentPrecisionData.observe(this@WhatTimeItIsFragment, Observer { setAdjustmentPrecision(it) })
            adjustmentModeData.observe(this@WhatTimeItIsFragment, Observer { setAdjustmentMode(it) })
            evaluationData.observe(this@WhatTimeItIsFragment, Observer { showEvaluation(it) })
            whatTimeItIsExerciseData.observe(this@WhatTimeItIsFragment, Observer { showExercise(it) })
            exercisesInSetData.observe(this@WhatTimeItIsFragment, Observer { setupExercisesInSet(it) })
            exercisesLeftData.observe(this@WhatTimeItIsFragment, Observer { showExercisesLeft(it) })
            finishData.observe(this@WhatTimeItIsFragment, Observer { showExerciseFinished(it) })
            fontData.observe(this@WhatTimeItIsFragment, Observer { setClockFont(it) })
            numeralsTypeData.observe(this@WhatTimeItIsFragment, Observer { setNumeralsType(it) })
            shownDigitsStepData.observe(this@WhatTimeItIsFragment, Observer { setShownDigitsStep(it) })
        }
    }

    private fun setAdjustmentMode(isAdjustmentMode: Boolean) {
        whatTimeItIsClock.setAdjustmentMode(isAdjustmentMode)
    }

    private fun setShownDigitsStep(step: Int) {
        whatTimeItIsClock.skippedDrawnDigits = step
    }

    private fun setAdjustmentPrecision(step: Int) {
        whatTimeItIsClock.adjustmentPrecision = step
    }

    private fun setNumeralsType(numeralsType: NumeralsType) {
        whatTimeItIsClock.numeralsType = numeralsType
    }

    private fun setClockFont(font: Font) {
        whatTimeItIsClock.font = font
    }

    private fun showExercisesLeft(exercisesLeft: Int) {
        exercisesCounterProgress.progress = exercisesCounterProgress.max - exercisesLeft
    }

    private fun setupExercisesInSet(exercisesInSet: Int) {
        counterMaxExercisesTxt.text = "$exercisesInSet"
        exercisesCounterProgress.max = exercisesInSet
    }

    private fun setupListeners() {
        whatTimeItIsAnswer0.setOnClickListener { whatTimeItIsViewModel.onAnswerClicked(0) }
        whatTimeItIsAnswer1.setOnClickListener { whatTimeItIsViewModel.onAnswerClicked(1) }
        whatTimeItIsAnswer2.setOnClickListener { whatTimeItIsViewModel.onAnswerClicked(2) }
        whatTimeItIsAnswer3.setOnClickListener { whatTimeItIsViewModel.onAnswerClicked(3) }
    }

    private fun showExercise(itIsExercise: WhatTimeItIsExercise) {
        whatTimeItIsClock.setTime(itIsExercise.question)
        val safeContext = context ?: return
        whatTimeItIsAnswer0.text = itIsExercise.answers[0].getLocalizedTime(safeContext)
        whatTimeItIsAnswer1.text = itIsExercise.answers[1].getLocalizedTime(safeContext)
        whatTimeItIsAnswer2.text = itIsExercise.answers[2].getLocalizedTime(safeContext)
        whatTimeItIsAnswer3.text = itIsExercise.answers[3].getLocalizedTime(safeContext)
    }

    private fun showEvaluation(evaluation: AnswerEvaluation) {
        playEvaluationSound(evaluation)
        val visibleDialog = activity?.let {
            when (evaluation) {
                AnswerEvaluation.SUCCESS -> dialogFactory
                        .createSuccessDialog(it) { whatTimeItIsViewModel.onEvaluationDialogDismissed() }
                AnswerEvaluation.FAILURE -> dialogFactory
                        .createFailureDialog(it) { whatTimeItIsViewModel.onEvaluationDialogDismissed() }
            }
        }
        setVisibleDialog(visibleDialog, true)
    }

    private fun showExerciseFinished(goodAnswersPercent: Int) {
        playExerciseFinishedSound()
        val visibleDialog = activity?.let {
            when (whatTimeItIsViewModel.mode) {
                ExerciseMode.LEARN -> dialogFactory
                        .createFinishLearnDialog(it) { whatTimeItIsViewModel.onEndDialogDismissed(whatTimeItIsClock) }
                ExerciseMode.TEST -> dialogFactory
                        .createFinishTestDialog(it, goodAnswersPercent) { whatTimeItIsViewModel.onEndDialogDismissed(whatTimeItIsClock) }
            }
        }
        setVisibleDialog(visibleDialog, false)
    }
}
