package com.szpejsoft.zegarek.ui.settings

import android.app.Activity
import android.view.View
import androidx.lifecycle.LiveData
import com.szpejsoft.zegarek.domain.model.ExercisesNumberSettings
import com.szpejsoft.zegarek.domain.model.Font
import com.szpejsoft.zegarek.domain.model.Level
import com.szpejsoft.zegarek.domain.model.NumeralsType

interface ISettingsViewModel {
    val musicOnLiveData: LiveData<Boolean>
    val soundOnLiveData: LiveData<Boolean>
    val numeralsLiveData: LiveData<NumeralsType>
    val fontLiveData: LiveData<Font>
    val levelLiveData: LiveData<Level>
    val exercisesNumberLiveData: LiveData<ExercisesNumberSettings>

    fun onMusicSwitched(musicOn: Boolean)
    fun onSoundSwitched(soundOn: Boolean)
    fun onLevelChosen(level: Level)
    fun onNumeralsChosen(type: NumeralsType)
    fun onExercisesNumberChosen(exercisesNumber: Int)
    fun onAboutClicked(aboutButton: View)
    fun onFontChosen(font: Font, activity: Activity)


}