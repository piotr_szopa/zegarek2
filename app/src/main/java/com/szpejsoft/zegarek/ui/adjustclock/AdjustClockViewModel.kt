package com.szpejsoft.zegarek.ui.adjustclock

import android.app.Application
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.navigation.Navigation
import com.szpejsoft.zegarek.base.SchedulersFacade
import com.szpejsoft.zegarek.base.SingleLiveEvent
import com.szpejsoft.zegarek.domain.model.*
import com.szpejsoft.zegarek.domain.model.exercises.AdjustClockExercise
import com.szpejsoft.zegarek.domain.usecase.adjustclock.IAdjustClockUseCase
import com.szpejsoft.zegarek.ui.base.BaseViewModel
import com.szpejsoft.zegarek.ui.base.ExerciseStrategy
import com.szpejsoft.zegarek.ui.base.LearnStrategy
import com.szpejsoft.zegarek.ui.base.TestStrategy

class AdjustClockViewModel(application: Application,
                           schedulersFacade: SchedulersFacade,
                           private val adjustClockUseCase: IAdjustClockUseCase
) : BaseViewModel(application, schedulersFacade), IAdjustClockViewModel {
    override val adjustmentModeData = MutableLiveData<Boolean>()

    override val adjustmentPrecisionData = MutableLiveData<Int>()
    override val evaluationData = SingleLiveEvent<AnswerEvaluation>()
    override val exerciseData = MutableLiveData<AdjustClockExercise>()
    override val exercisesInSetData = MutableLiveData<Int>()
    override val exercisesLeftData = MutableLiveData<Int>()
    override val finishData = SingleLiveEvent<Int>()
    override val fontData = MutableLiveData<Font>()
    override var mode: ExerciseMode = ExerciseMode.LEARN
        set(value) {
            field = value
            strategy = when (value) {
                ExerciseMode.LEARN -> LearnStrategy()
                ExerciseMode.TEST -> TestStrategy<AdjustClockExercise>()
                        .apply { this.exercises = exerciseList.size }
            }
        }
    override val numeralsTypeData = MutableLiveData<NumeralsType>()
    override val shownDigitsStepData = MutableLiveData<Int>()

    private var currentExercise: AdjustClockExercise? = null
        set(value) {
            field = value
            exerciseData.value = value
        }
    private val exerciseList = mutableListOf<AdjustClockExercise>()
    private var strategy: ExerciseStrategy<AdjustClockExercise>? = null

    init {
        adjustClockUseCase.getExercises().subscribeBy { exercises -> setupExercises(exercises) }
        adjustClockUseCase.getClockSettings().subscribeBy { clockSettings -> setupClock(clockSettings) }
    }

    override fun onCheckClicked(answer: Instant) {
        val safeStrategy = strategy ?: throw IllegalStateException("strategy must be set")
        if (isProperAnswer(answer)) {
            safeStrategy.handleProperAnswer(exerciseList, currentExercise!!, finishData, evaluationData)
        } else {
            safeStrategy.handleWrongAnswer(exerciseList, currentExercise!!, finishData, evaluationData)
        }
        exercisesLeftData.value = exerciseList.size
    }

    override fun onEvaluationDialogDismissed() {
        currentExercise = getRandomExercise()
    }

    override fun onEndDialogDismissed(view: View) {
        Navigation.findNavController(view).navigateUp()
    }

    private fun setupExercises(exercises: List<AdjustClockExercise>) {
        this.exerciseList.apply {
            clear()
            addAll(exercises)
        }
        exercisesInSetData.value = exerciseList.size
        exercisesLeftData.value = exerciseList.size
        currentExercise = getRandomExercise()
        val safeStrategy = strategy ?: return
        if (safeStrategy is TestStrategy) {
            safeStrategy.exercises = exerciseList.size
        }
    }

    private fun getRandomExercise(): AdjustClockExercise = exerciseList[nextInt(exerciseList.size)]

    private fun setupClock(clockSettings: ClockSettings) {
        clockSettings.apply {
            adjustmentModeData.value = clockSettings.isAdjustmentMode
            adjustmentPrecisionData.value = adjustmentPrecision.step
            fontData.value = font
            numeralsTypeData.value = numeralsType
            shownDigitsStepData.value = shownDigits.step
        }
    }

    private fun isProperAnswer(answer: Instant): Boolean {
        val current = currentExercise ?: return false
        return current.question.equalsMod12(answer)
    }
}
