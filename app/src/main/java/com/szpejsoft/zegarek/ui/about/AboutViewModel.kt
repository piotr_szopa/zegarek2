package com.szpejsoft.zegarek.ui.about

import android.app.Application
import android.text.Spanned
import androidx.lifecycle.MutableLiveData
import com.szpejsoft.zegarek.R
import com.szpejsoft.zegarek.base.SchedulersFacade
import com.szpejsoft.zegarek.ui.base.BaseViewModel

class AboutViewModel(application: Application,
                     schedulersFacade: SchedulersFacade
) : BaseViewModel(application, schedulersFacade), IAboutViewModel {

    override val fontDescriptionData = MutableLiveData<Spanned>()
    override val musicDescriptionData = MutableLiveData<Spanned>()
    override val soundDescriptionData = MutableLiveData<Spanned>()

    init {
        musicDescriptionData.value = getSpannable(R.string.about_music_content)
        soundDescriptionData.value = getSpannable(R.string.about_sound_content)
        fontDescriptionData.value = getSpannable(R.string.about_font_content)
    }


}