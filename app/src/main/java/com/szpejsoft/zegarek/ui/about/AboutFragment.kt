package com.szpejsoft.zegarek.ui.about

import android.os.Bundle
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.lifecycle.Observer
import com.szpejsoft.zegarek.R
import com.szpejsoft.zegarek.ui.base.BaseFragment
import kotlinx.android.synthetic.main.about_fragment.*
import org.koin.android.viewmodel.ext.android.viewModel

class AboutFragment : BaseFragment() {
    private val aboutViewModel: IAboutViewModel by viewModel<AboutViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.about_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(false)
        setupDescriptions()
    }

    override fun bindViewModel() {
        aboutViewModel.apply {
            musicDescriptionData.observe(viewLifecycleOwner, Observer { setMusicDescription(it) })
            soundDescriptionData.observe(viewLifecycleOwner, Observer { setSoundDescription(it) })
            fontDescriptionData.observe(viewLifecycleOwner, Observer { setFontDescription(it) })
        }
    }

    private fun setupDescriptions() {
        setupDescription(aboutMusicDescription)
        setupDescription(aboutSoundDescription)
        setupDescription(aboutFontDescription)
    }

    private fun setupDescription(descriptionView: AppCompatTextView) {
        descriptionView.movementMethod = LinkMovementMethod.getInstance()
    }

    private fun setMusicDescription(description: Spanned) {
        aboutMusicDescription.text = description
    }

    private fun setSoundDescription(description: Spanned) {
        aboutSoundDescription.text = description
    }

    private fun setFontDescription(description: Spanned) {
        aboutFontDescription.text = description
    }


}
