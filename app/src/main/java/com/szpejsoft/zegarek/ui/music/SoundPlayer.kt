package com.szpejsoft.zegarek.ui.music

interface SoundPlayer {
    var play: Boolean
    fun playSuccess()
    fun playFailure()
    fun playLevelEnd()
}