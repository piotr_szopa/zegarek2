package com.szpejsoft.zegarek.ui.main

import android.app.Application
import com.szpejsoft.zegarek.base.SchedulersFacade
import com.szpejsoft.zegarek.data.font.IFontRepository
import com.szpejsoft.zegarek.domain.model.Font
import com.szpejsoft.zegarek.ui.base.BaseViewModel

class MainActivityViewModel(application: Application,
                            schedulersFacade: SchedulersFacade,
                            private val fontRepository: IFontRepository
) : BaseViewModel(application, schedulersFacade), IMainActivityViewModel {

    override fun getFont(): Font = fontRepository.fontObservable.blockingFirst()
}