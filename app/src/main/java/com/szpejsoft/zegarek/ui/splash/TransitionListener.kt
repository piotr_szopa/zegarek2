package com.szpejsoft.zegarek.ui.splash

import androidx.transition.Transition

class TransitionListener(private val endAction: (Transition) -> Unit) : Transition.TransitionListener {
    override fun onTransitionEnd(transition: Transition) {
        endAction.invoke(transition)
    }

    override fun onTransitionResume(transition: Transition) {
        //dunuthin
    }

    override fun onTransitionPause(transition: Transition) {
        //dunuthin
    }

    override fun onTransitionCancel(transition: Transition) {
        //dunuthin
    }

    override fun onTransitionStart(transition: Transition) {
        //dunuthin
    }
}