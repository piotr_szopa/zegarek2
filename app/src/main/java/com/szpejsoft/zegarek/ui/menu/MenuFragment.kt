package com.szpejsoft.zegarek.ui.menu

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.navigation.Navigation.findNavController
import com.szpejsoft.zegarek.R
import com.szpejsoft.zegarek.domain.model.ExerciseMode
import com.szpejsoft.zegarek.domain.model.exercises.ChosenExercise
import com.szpejsoft.zegarek.ui.base.BaseFragment
import kotlinx.android.synthetic.main.menu_fragment.*
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber


class MenuFragment : BaseFragment() {

    private val menuViewModel: IMenuViewModel by viewModel<MenuViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.menu_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindOnClickListeners()
        Timber.d("ptsz height ${activity!!.resources.configuration.screenHeightDp}")
    }

    override fun bindViewModel() {
        menuViewModel.showRateAppDialogObservable.observe(viewLifecycleOwner, Observer { showRateAppDialog() })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.menu_settings) {
            findNavController(menuFragmentRootView).navigate(R.id.menu_to_settings)
        }
        return super.onOptionsItemSelected(item)
    }

    private fun bindOnClickListeners() {
        whatTimeItIsButton.setOnClickListener {
            showChooseLearnTestDialog(ChosenExercise.WHAT_TIME_IT_IS)
        }
        whatTimeWillBeButton.setOnClickListener {
            showChooseLearnTestDialog(ChosenExercise.WHAT_TIME_WILL_BE)
        }
        adjustClockButton.setOnClickListener {
            showChooseLearnTestDialog(ChosenExercise.ADJUST_CLOCK)
        }
        timePassedButton.setOnClickListener {
            showChooseLearnTestDialog(ChosenExercise.TIME_PASSED)
        }
    }

    private fun showRateAppDialog() {
        activity?.let {
            dialogFactory.createShowRateAppDialog(it,
                    rateLaterAction = {
                        //dunuthin
                    },
                    dontShowDialogAction = { saveDontShowRateAppDialog() },
                    rateNowAction = { rateApp() })
                    .show()
        } ?: throw IllegalStateException("activity MUST not be null")
    }

    private fun saveDontShowRateAppDialog() {
        menuViewModel.saveDontShowRateAppDialog()
    }

    private fun rateApp() {
        activity?.let { act ->
            saveDontShowRateAppDialog()
            try {
                tryRateInGooglePlay(act)
            } catch (e: ActivityNotFoundException) {
                try {
                    tryRateInBrowser(act)
                } catch (e: ActivityNotFoundException) {
                    Toast.makeText(act, R.string.dialog_rate_app_cant_rate, Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun tryRateInGooglePlay(act: FragmentActivity) =
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=${act.packageName}")))

    private fun tryRateInBrowser(act: FragmentActivity) =
            startActivity(getIntent("https://play.google.com/store/apps/details?id=${act.packageName}"))

    private fun getIntent(url: String): Intent = Intent(Intent.ACTION_VIEW, Uri.parse(url)).also {
        it.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK or
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT)
    }

    private fun showChooseLearnTestDialog(chosenExercise: ChosenExercise) = activity?.let {
        dialogFactory.createChooseModeDialog(it) { mode -> navigateTo(chosenExercise, mode) }
                .show()
    }

    private fun navigateTo(chosenExercise: ChosenExercise, mode: ExerciseMode) = when (chosenExercise) {
        ChosenExercise.ADJUST_CLOCK -> navigateToAdjustClock(mode)
        ChosenExercise.TIME_PASSED -> navigateToTimePassed(mode)
        ChosenExercise.WHAT_TIME_IT_IS -> navigateToWhatTimeItIs(mode)
        ChosenExercise.WHAT_TIME_WILL_BE -> navigateToWhatTimeWillBe(mode)
    }

    private fun navigateToWhatTimeWillBe(mode: ExerciseMode) {
        findNavController(menuFragmentRootView).navigate(MenuFragmentDirections.menuToWhatTimeWillBe(mode))
    }

    private fun navigateToAdjustClock(mode: ExerciseMode) {
        findNavController(menuFragmentRootView).navigate(MenuFragmentDirections.menuToAdjustClock(mode))
    }

    private fun navigateToWhatTimeItIs(mode: ExerciseMode) {
        findNavController(menuFragmentRootView).navigate(MenuFragmentDirections.menuToWhatTimeItIs(mode))
    }

    private fun navigateToTimePassed(mode: ExerciseMode) {
        findNavController(menuFragmentRootView).navigate(MenuFragmentDirections.menuToTimePassed(mode))
    }
}
