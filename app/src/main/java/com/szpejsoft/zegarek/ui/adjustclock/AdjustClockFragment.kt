package com.szpejsoft.zegarek.ui.adjustclock

import android.os.Bundle
import android.view.*
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.szpejsoft.zegarek.R
import com.szpejsoft.zegarek.base.SchedulersFacade
import com.szpejsoft.zegarek.base.setValueChanger
import com.szpejsoft.zegarek.domain.model.*
import com.szpejsoft.zegarek.domain.model.exercises.AdjustClockExercise
import com.szpejsoft.zegarek.domain.model.exercises.ChosenExercise
import com.szpejsoft.zegarek.ui.base.BaseFragment
import kotlinx.android.synthetic.main.adjust_clock_fragment.*
import kotlinx.android.synthetic.main.exercises_counter.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class AdjustClockFragment : BaseFragment() {
    private val adjustClockViewModel: IAdjustClockViewModel by viewModel<AdjustClockViewModel>()
    private val schedulersFacade: SchedulersFacade by inject()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.adjust_clock_fragment, container, false)

    override fun bindViewModel() {
        adjustClockViewModel.mode = AdjustClockFragmentArgs.fromBundle(arguments!!).mode
        setupObservers()
        setupListeners()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.exercise_fragment_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item?.let {
            if (item.itemId == R.id.help) {
                val directions = AdjustClockFragmentDirections.actionAdjustClockFragmentToHelpFragment(ChosenExercise.ADJUST_CLOCK)
                Navigation.findNavController(adjustClockClockView)
                        .navigate(directions)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupObservers() {
        adjustClockViewModel.apply {
            adjustmentPrecisionData.observe(this@AdjustClockFragment, Observer { setAdjustmentPrecision(it) })
            adjustmentModeData.observe(this@AdjustClockFragment, Observer { setAdjustmentMode(it) })
            evaluationData.observe(this@AdjustClockFragment, Observer { showEvaluation(it) })
            exerciseData.observe(this@AdjustClockFragment, Observer { showExercise(it) })
            exercisesInSetData.observe(this@AdjustClockFragment, Observer { setupExercisesInSet(it) })
            exercisesLeftData.observe(this@AdjustClockFragment, Observer { showExercisesLeft(it) })
            finishData.observe(this@AdjustClockFragment, Observer { showExerciseFinished(it) })
            fontData.observe(this@AdjustClockFragment, Observer { setClockFont(it) })
            numeralsTypeData.observe(this@AdjustClockFragment, Observer { setNumeralsType(it) })
            shownDigitsStepData.observe(this@AdjustClockFragment, Observer { setShownDigitsStep(it) })
        }
    }

    private fun setupListeners() {
        adjustClockCheckAnswerBtn.setOnClickListener { adjustClockViewModel.onCheckClicked(adjustClockClockView.getTime()) }
        //below buttons only in tablet layout
        adjustClockCheckAnswerBtnDuplicate?.setOnClickListener { adjustClockViewModel.onCheckClicked(adjustClockClockView.getTime()) }
        adjustClockIncreaseMinuteBtn?.setValueChanger(schedulersFacade) { adjustClockClockView.increaseMinute() }
        adjustClockDecreaseMinuteBtn?.setValueChanger(schedulersFacade) { adjustClockClockView.decreaseMinute() }
        adjustClockIncreaseHourBtn?.setValueChanger(schedulersFacade) { adjustClockClockView.increaseHour() }
        adjustClockDecreaseHourBtn?.setValueChanger(schedulersFacade) { adjustClockClockView.decreaseHour() }

    }

    private fun showExercise(exercise: AdjustClockExercise) {
        adjustClockClockView.setTime(exercise.initialTime)
        adjustClockExerciseTxt.text = getString(R.string.adjust_time_set_time, exercise.question.getLocalizedTime(activity!!))
    }

    private fun setAdjustmentMode(isAdjustmentMode: Boolean) {
        adjustClockClockView.setAdjustmentMode(isAdjustmentMode)
    }

    private fun setShownDigitsStep(step: Int) {
        adjustClockClockView.skippedDrawnDigits = step
    }

    private fun setAdjustmentPrecision(step: Int) {
        adjustClockClockView.adjustmentPrecision = step
        adjustClockIncreaseMinuteBtn?.text = getString(R.string.adjust_clock_increase_minute_text, step)
        adjustClockDecreaseMinuteBtn?.text = getString(R.string.adjust_clock_decrease_minute_text, step)
    }

    private fun setNumeralsType(numeralsType: NumeralsType) {
        adjustClockClockView.numeralsType = numeralsType
    }

    private fun setClockFont(font: Font) {
        adjustClockClockView.font = font
    }

    private fun showExercisesLeft(exercisesLeft: Int) {
        exercisesCounterProgress.progress = exercisesCounterProgress.max - exercisesLeft
    }

    private fun setupExercisesInSet(exercisesInSet: Int) {
        counterMaxExercisesTxt.text = "$exercisesInSet"
        exercisesCounterProgress.max = exercisesInSet
    }

    private fun showEvaluation(evaluation: AnswerEvaluation) {
        playEvaluationSound(evaluation)
        val visibleDialog = activity?.let {
            when (evaluation) {
                AnswerEvaluation.SUCCESS -> dialogFactory
                        .createSuccessDialog(it) { adjustClockViewModel.onEvaluationDialogDismissed() }

                AnswerEvaluation.FAILURE -> dialogFactory
                        .createFailureDialog(it) { adjustClockViewModel.onEvaluationDialogDismissed() }
            }
        }
        setVisibleDialog(visibleDialog, true)
    }

    private fun showExerciseFinished(goodAnswersPercent: Int) {
        playExerciseFinishedSound()
        val visibleDialog = activity?.let {
            when (adjustClockViewModel.mode) {
                ExerciseMode.LEARN -> dialogFactory
                        .createFinishLearnDialog(it) { adjustClockViewModel.onEndDialogDismissed(adjustClockClockView) }
                ExerciseMode.TEST -> dialogFactory
                        .createFinishTestDialog(it, goodAnswersPercent) { adjustClockViewModel.onEndDialogDismissed(adjustClockClockView) }
            }
        }
        setVisibleDialog(visibleDialog, false)
    }
}