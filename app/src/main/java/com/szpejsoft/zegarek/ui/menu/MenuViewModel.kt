package com.szpejsoft.zegarek.ui.menu

import android.app.Application
import com.szpejsoft.zegarek.base.SchedulersFacade
import com.szpejsoft.zegarek.base.SingleLiveEvent
import com.szpejsoft.zegarek.data.rateapp.IRateAppRepository
import com.szpejsoft.zegarek.ui.base.BaseViewModel

class MenuViewModel(application: Application,
                    schedulersFacade: SchedulersFacade,
                    private val rateAppRepository: IRateAppRepository) : BaseViewModel(application, schedulersFacade), IMenuViewModel {


    override val showRateAppDialogObservable: SingleLiveEvent<Unit> = SingleLiveEvent()

    init {
        rateAppRepository.showRateAppDialogObservable.subscribeBy { unit -> showRateAppDialogObservable.value = unit }
    }

    override fun saveDontShowRateAppDialog() {
        rateAppRepository.setShowRateAppDialog(false)
    }


}