package com.szpejsoft.zegarek.ui.menu

import androidx.lifecycle.LiveData

interface IMenuViewModel {
    fun saveDontShowRateAppDialog()

    val showRateAppDialogObservable: LiveData<Unit>


}