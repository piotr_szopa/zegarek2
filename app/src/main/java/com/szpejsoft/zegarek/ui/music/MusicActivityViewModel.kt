package com.szpejsoft.zegarek.ui.music

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.szpejsoft.zegarek.base.SchedulersFacade
import com.szpejsoft.zegarek.data.music.IMusicRepository
import com.szpejsoft.zegarek.ui.base.BaseViewModel

class MusicActivityViewModel(application: Application,
                             schedulersFacade: SchedulersFacade,
                             musicRepository: IMusicRepository
) : BaseViewModel(application, schedulersFacade), IMusicActivityViewModel {

    override val musicLiveData = MutableLiveData<Boolean>()
    override val soundLiveData = MutableLiveData<Boolean>()

    init {
        musicRepository.musicStateObservable.subscribeBy { musicLiveData.value = it }
        musicRepository.soundStateObservable.subscribeBy { soundLiveData.value = it }

    }
}