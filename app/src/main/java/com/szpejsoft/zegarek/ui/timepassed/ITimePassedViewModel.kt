package com.szpejsoft.zegarek.ui.timepassed

import android.view.View
import androidx.lifecycle.LiveData
import com.szpejsoft.zegarek.domain.model.AnswerEvaluation
import com.szpejsoft.zegarek.domain.model.ExerciseMode
import com.szpejsoft.zegarek.domain.model.exercises.TimePassedExercise

interface ITimePassedViewModel {
    val exercisesInSetData: LiveData<Int>
    val exercisesLeftData: LiveData<Int>
    val exerciseData: LiveData<TimePassedExercise>
    val evaluationData: LiveData<AnswerEvaluation>
    val finishData: LiveData<Int>

    var mode: ExerciseMode

    fun onAnswerClicked(answerOrdinal: Int)
    fun onEvaluationDialogDismissed()
    //TODO maybe implementation in BaseViewModel?
    //TODO change view to fragment
    fun onEndDialogDismissed(view: View)
}