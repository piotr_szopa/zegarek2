package com.szpejsoft.zegarek.ui.adjustclock

import android.view.View
import androidx.lifecycle.LiveData
import com.szpejsoft.zegarek.domain.model.*
import com.szpejsoft.zegarek.domain.model.exercises.AdjustClockExercise

interface IAdjustClockViewModel {
    val exerciseData: LiveData<AdjustClockExercise>
    val numeralsTypeData: LiveData<NumeralsType>
    val adjustmentPrecisionData: LiveData<Int>
    val shownDigitsStepData: LiveData<Int>

    var mode: ExerciseMode
    //TODO these LiveDatas will be probably moved to BaseViewModel
    val adjustmentModeData: LiveData<Boolean>
    val evaluationData: LiveData<AnswerEvaluation>
    val exercisesInSetData: LiveData<Int>
    val exercisesLeftData: LiveData<Int>
    val finishData: LiveData<Int>
    val fontData: LiveData<Font>

    fun onCheckClicked(answer: Instant)
    fun onEvaluationDialogDismissed()
    //TODO change view to fragment
    fun onEndDialogDismissed(view: View)
}