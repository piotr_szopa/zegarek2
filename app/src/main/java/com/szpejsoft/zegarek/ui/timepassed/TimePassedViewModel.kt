package com.szpejsoft.zegarek.ui.timepassed

import android.app.Application
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.navigation.Navigation
import com.szpejsoft.zegarek.base.SchedulersFacade
import com.szpejsoft.zegarek.base.SingleLiveEvent
import com.szpejsoft.zegarek.domain.model.AnswerEvaluation
import com.szpejsoft.zegarek.domain.model.ExerciseMode
import com.szpejsoft.zegarek.domain.model.exercises.TimePassedExercise
import com.szpejsoft.zegarek.domain.usecase.timepassed.ITimePassedUseCase
import com.szpejsoft.zegarek.ui.base.BaseViewModel
import com.szpejsoft.zegarek.ui.base.ExerciseStrategy
import com.szpejsoft.zegarek.ui.base.LearnStrategy
import com.szpejsoft.zegarek.ui.base.TestStrategy

class TimePassedViewModel(application: Application,
                          schedulersFacade: SchedulersFacade,
                          private val timePassedUseCase: ITimePassedUseCase
) : BaseViewModel(application, schedulersFacade), ITimePassedViewModel {
    override val exercisesInSetData = MutableLiveData<Int>()
    override val exercisesLeftData = MutableLiveData<Int>()
    override val exerciseData = MutableLiveData<TimePassedExercise>()
    override val evaluationData = SingleLiveEvent<AnswerEvaluation>()
    override val finishData = SingleLiveEvent<Int>()

    private var strategy: ExerciseStrategy<TimePassedExercise>? = null

    private var currentExercise: TimePassedExercise? = null
        set(value) {
            field = value
            exerciseData.value = value
        }
    private val exerciseList = mutableListOf<TimePassedExercise>()
    override var mode: ExerciseMode = ExerciseMode.LEARN
        set(value) {
            field = value
            strategy = when (value) {
                ExerciseMode.LEARN -> LearnStrategy()
                ExerciseMode.TEST -> TestStrategy<TimePassedExercise>()
                        .apply { this.exercises = exerciseList.size }
            }
        }

    init {
        timePassedUseCase.getExercises()
                .subscribeBy { exercises -> setupExercises(exercises) }
    }

    override fun onAnswerClicked(answerOrdinal: Int) {
        val safeStrategy = strategy ?: throw IllegalStateException("strategy must be set")
        if (isProperAnswer(answerOrdinal)) {
            safeStrategy.handleProperAnswer(exerciseList, currentExercise!!, finishData, evaluationData)
        } else {
            safeStrategy.handleWrongAnswer(exerciseList, currentExercise!!, finishData, evaluationData)
        }
        exercisesLeftData.value = exerciseList.size
    }

    private fun isProperAnswer(answerOrdinal: Int): Boolean {
        val safeExercise = currentExercise ?: return false
        return safeExercise.answers[answerOrdinal] == safeExercise.properAnswer
    }

    override fun onEvaluationDialogDismissed() {
        currentExercise = getRandomExercise()
    }

    override fun onEndDialogDismissed(view: View) {
        Navigation.findNavController(view).navigateUp()
    }

    private fun getRandomExercise(): TimePassedExercise = exerciseList[nextInt(exerciseList.size)]

    private fun setupExercises(exercises: List<TimePassedExercise>) {
        exerciseList.apply {
            clear()
            addAll(exercises)
        }
        exercisesInSetData.value = exerciseList.size
        exercisesLeftData.value = exerciseList.size
        val safeStrategy = strategy ?: return
        if (safeStrategy is TestStrategy) {
            safeStrategy.exercises = exerciseList.size
        }
        currentExercise = getRandomExercise()
    }

}