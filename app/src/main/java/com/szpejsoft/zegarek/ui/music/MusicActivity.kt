package com.szpejsoft.zegarek.ui.music

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.szpejsoft.zegarek.R
import org.koin.android.viewmodel.ext.android.viewModel

abstract class MusicActivity : AppCompatActivity() {

    private val musicActivityViewModel: IMusicActivityViewModel by viewModel<MusicActivityViewModel>()

    private var musicSoundService: SoundService? = null
    private var serviceConnected = false

    private val connection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, binder: IBinder) {
            val localBinder = binder as SoundService.SoundServiceBinder
            musicSoundService = localBinder.soundService
            serviceConnected = true
            if (musicActivityViewModel.musicLiveData.value == true) {
                musicSoundService?.playMusic()
            }
        }

        override fun onServiceDisconnected(name: ComponentName) {
            musicSoundService = null
            serviceConnected = false
        }
    }

    val soundService = object : SoundPlayer {
        override var play: Boolean = true
        override fun playSuccess() {
            if (play) musicSoundService?.playSuccess()
        }

        override fun playFailure() {
            if (play) musicSoundService?.playFailure()
        }

        override fun playLevelEnd() {
            if (play) musicSoundService?.playLevelEnd()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeViewModel()
    }

    override fun onStart() {
        super.onStart()
        val i = Intent(this, SoundService::class.java)
        if (!serviceConnected) {
            bindService(i, connection, Context.BIND_AUTO_CREATE)
            serviceConnected = true
        }
        if (musicActivityViewModel.musicLiveData.value == true) {
            musicSoundService?.playMusic()
        }
    }

    override fun onStop() {
        super.onStop()
        if (serviceConnected) {
            unbindService(connection)
            serviceConnected = false
        }
        super.onPause()
    }

    private fun adjustSound(soundOn: Boolean) {
        soundService.play = soundOn
    }

    private fun adjustMusic(musicOn: Boolean) {
        if (musicOn) {
            musicSoundService?.playMusic()
        } else
            musicSoundService?.stopPlayingMusic()
    }

    private fun observeViewModel() {
        musicActivityViewModel.musicLiveData.observe(this, Observer { adjustMusic(it) })
        musicActivityViewModel.soundLiveData.observe(this, Observer { adjustSound(it) })
    }

    protected fun isPhone(): Boolean = resources.getBoolean(R.bool.isPhone)

}