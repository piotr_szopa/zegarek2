package com.szpejsoft.zegarek.ui.music

import androidx.lifecycle.LiveData

interface IMusicActivityViewModel {
    val musicLiveData: LiveData<Boolean>
    val soundLiveData: LiveData<Boolean>
}