package com.szpejsoft.zegarek.ui.timepassed

import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.*
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.szpejsoft.zegarek.R
import com.szpejsoft.zegarek.domain.model.AnswerEvaluation
import com.szpejsoft.zegarek.domain.model.ExerciseMode
import com.szpejsoft.zegarek.domain.model.exercises.ChosenExercise
import com.szpejsoft.zegarek.domain.model.exercises.TimePassedExercise
import com.szpejsoft.zegarek.domain.model.getLocalizedStringTwoLines
import com.szpejsoft.zegarek.domain.model.getLocalizedTime
import com.szpejsoft.zegarek.ui.base.BaseFragment
import kotlinx.android.synthetic.main.exercises_counter.*
import kotlinx.android.synthetic.main.time_passed_fragment.*
import org.koin.android.viewmodel.ext.android.viewModel

class TimePassedFragment : BaseFragment() {
    private val timePassedViewModel: ITimePassedViewModel by viewModel<TimePassedViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.time_passed_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        timePassedExerciseDescription.movementMethod = ScrollingMovementMethod()
    }

    override fun bindViewModel() {
        timePassedViewModel.mode = TimePassedFragmentArgs.fromBundle(arguments!!).mode
        setupObservers()
        setupListeners()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.exercise_fragment_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item?.let {
            if (item.itemId == R.id.help) {
                val directions = TimePassedFragmentDirections.actionTimePassedFragmentToHelpFragment(ChosenExercise.TIME_PASSED)
                Navigation.findNavController(timePassedExerciseDescription)
                        .navigate(directions)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupListeners() {
        timePassedAnswer0.setOnClickListener { timePassedViewModel.onAnswerClicked(0) }
        timePassedAnswer1.setOnClickListener { timePassedViewModel.onAnswerClicked(1) }
        timePassedAnswer2.setOnClickListener { timePassedViewModel.onAnswerClicked(2) }
        timePassedAnswer3.setOnClickListener { timePassedViewModel.onAnswerClicked(3) }
    }

    private fun setupObservers() {
        timePassedViewModel.apply {
            exercisesInSetData.observe(this@TimePassedFragment, Observer { setupExercisesInSet(it) })
            exercisesLeftData.observe(this@TimePassedFragment, Observer { setupExercisesLeft(it) })
            exerciseData.observe(this@TimePassedFragment, Observer { setupExercise(it) })
            evaluationData.observe(this@TimePassedFragment, Observer { showEvaluationDialog(it) })
            finishData.observe(this@TimePassedFragment, Observer { showFinishDialog(it) })
        }
    }

    private fun setupExercisesInSet(exercisesInSet: Int) {
        counterMaxExercisesTxt.text = "$exercisesInSet"
        exercisesCounterProgress.max = exercisesInSet
    }

    private fun setupExercisesLeft(exercisesLeft: Int) {
        exercisesCounterProgress.progress = exercisesCounterProgress.max - exercisesLeft
    }

    private fun setupExercise(exercise: TimePassedExercise) {
        activity?.let {
            timePassedExerciseDescription.text = getString(R.string.how_much_time_passed_exercise_description,
                    exercise.startTime.getLocalizedTime(it),
                    exercise.endTime.getLocalizedTime(it))
            timePassedAnswer0.text = exercise.answers[0].getLocalizedStringTwoLines(it)
            timePassedAnswer1.text = exercise.answers[1].getLocalizedStringTwoLines(it)
            timePassedAnswer2.text = exercise.answers[2].getLocalizedStringTwoLines(it)
            timePassedAnswer3.text = exercise.answers[3].getLocalizedStringTwoLines(it)
        } ?: throw IllegalStateException("activity must not be null")
    }

    private fun showEvaluationDialog(evaluation: AnswerEvaluation) {
        playEvaluationSound(evaluation)
        val visibleDialog = activity?.let {
            when (evaluation) {
                AnswerEvaluation.SUCCESS -> dialogFactory
                        .createSuccessDialog(it) { timePassedViewModel.onEvaluationDialogDismissed() }
                AnswerEvaluation.FAILURE -> dialogFactory
                        .createFailureDialog(it) { timePassedViewModel.onEvaluationDialogDismissed() }
            }
        }
        setVisibleDialog(visibleDialog, true)
    }

    private fun showFinishDialog(goodAnswersPercent: Int) {
        playExerciseFinishedSound()
        val visibleDialog = activity?.let {
            when (timePassedViewModel.mode) {
                ExerciseMode.LEARN -> dialogFactory
                        .createFinishLearnDialog(it) { timePassedViewModel.onEndDialogDismissed(timePassedExerciseDescription) }
                ExerciseMode.TEST -> dialogFactory
                        .createFinishTestDialog(it, goodAnswersPercent) { timePassedViewModel.onEndDialogDismissed(timePassedExerciseDescription) }
            }
        }
        setVisibleDialog(visibleDialog, false)
    }
}
