package com.szpejsoft.zegarek.ui.dialogs

import android.app.Activity
import android.app.Dialog
import com.szpejsoft.zegarek.domain.model.*

interface IDialogFactory {
    fun createSuccessDialog(activity: Activity, dismissAction: () -> Unit): Dialog
    fun createFailureDialog(activity: Activity, dismissAction: () -> Unit): Dialog
    fun createFinishLearnDialog(activity: Activity, dismissAction: () -> Unit): Dialog
    fun createFinishTestDialog(activity: Activity, scorePercent: Int, dismissAction: () -> Unit): Dialog

    fun createChooseModeDialog(activity: Activity, action: (ExerciseMode) -> Unit): Dialog
    fun createChooseExercisesNumberDialog(activity: Activity, numberSettings: ExercisesNumberSettings, font: Font, action: (Int) -> Unit): Dialog
    fun createSelectLevelDialog(activity: Activity, currentLevel: Level, action: (Level) -> Unit): Dialog
    fun createSelectFontDialog(activity: Activity, currentFont: Font, action: (Font) -> Unit): Dialog
    fun createSelectNumeralsDialog(activity: Activity, currentNumerals: NumeralsType, action: (NumeralsType) -> Unit): Dialog

    fun createShowRateAppDialog(activity: Activity, rateLaterAction: () -> Unit, dontShowDialogAction: () -> Unit, rateNowAction: () -> Unit): Dialog

}