package com.szpejsoft.zegarek.ui.whattimewillbe

import android.os.Bundle
import android.view.*
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.szpejsoft.zegarek.R
import com.szpejsoft.zegarek.domain.model.AnswerEvaluation
import com.szpejsoft.zegarek.domain.model.ExerciseMode
import com.szpejsoft.zegarek.domain.model.exercises.ChosenExercise
import com.szpejsoft.zegarek.domain.model.exercises.WhatTimeWillBeExercise
import com.szpejsoft.zegarek.domain.model.getLocalizedStringOneLine
import com.szpejsoft.zegarek.domain.model.getLocalizedTime
import com.szpejsoft.zegarek.ui.base.BaseFragment
import com.szpejsoft.zegarek.ui.timepassed.TimePassedFragmentArgs
import kotlinx.android.synthetic.main.exercises_counter.*
import kotlinx.android.synthetic.main.what_time_will_be_fragment.*
import org.koin.android.viewmodel.ext.android.viewModel

class WhatTimeWillBeFragment : BaseFragment() {
    private val whatTimeWillBeViewModel: IWhatTimeWillBeViewModel by viewModel<WhatTimeWillBeViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.what_time_will_be_fragment, container, false)

    override fun bindViewModel() {
        whatTimeWillBeViewModel.mode = TimePassedFragmentArgs.fromBundle(arguments!!).mode
        setupObservers()
        setupListeners()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.exercise_fragment_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item?.let {
            if (item.itemId == R.id.help) {
                val directions = WhatTimeWillBeFragmentDirections.actionWhatTimeWillBeFragmentToHelpFragment(ChosenExercise.WHAT_TIME_WILL_BE)
                Navigation.findNavController(whatTimeWillBeExerciseDescription)
                        .navigate(directions)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupListeners() {
        whatTimeWillBeAnswer0.setOnClickListener { whatTimeWillBeViewModel.onAnswerClicked(0) }
        whatTimeWillBeAnswer1.setOnClickListener { whatTimeWillBeViewModel.onAnswerClicked(1) }
        whatTimeWillBeAnswer2.setOnClickListener { whatTimeWillBeViewModel.onAnswerClicked(2) }
        whatTimeWillBeAnswer3.setOnClickListener { whatTimeWillBeViewModel.onAnswerClicked(3) }
    }

    private fun setupObservers() {
        whatTimeWillBeViewModel.apply {
            exercisesInSetData.observe(this@WhatTimeWillBeFragment, Observer { setupExercisesInSet(it) })
            exercisesLeftData.observe(this@WhatTimeWillBeFragment, Observer { setupExercisesLeft(it) })
            exerciseData.observe(this@WhatTimeWillBeFragment, Observer { setupExercise(it) })
            evaluationData.observe(this@WhatTimeWillBeFragment, Observer { showEvaluationDialog(it) })
            finishData.observe(this@WhatTimeWillBeFragment, Observer { showFinishDialog(it) })
        }
    }

    private fun setupExercisesInSet(exercisesInSet: Int) {
        counterMaxExercisesTxt.text = "$exercisesInSet"
        exercisesCounterProgress.max = exercisesInSet
    }

    private fun setupExercisesLeft(exercisesLeft: Int) {
        exercisesCounterProgress.progress = exercisesCounterProgress.max - exercisesLeft
    }

    private fun setupExercise(exercise: WhatTimeWillBeExercise) {
        activity?.apply {
            whatTimeWillBeExerciseDescription.text = getString(R.string.what_time_will_be_exercise_description,
                    exercise.startTime.getLocalizedTime(this),
                    exercise.difference.getLocalizedStringOneLine(this))
            whatTimeWillBeAnswer0.text = exercise.answers[0].getLocalizedTime(this)
            whatTimeWillBeAnswer1.text = exercise.answers[1].getLocalizedTime(this)
            whatTimeWillBeAnswer2.text = exercise.answers[2].getLocalizedTime(this)
            whatTimeWillBeAnswer3.text = exercise.answers[3].getLocalizedTime(this)
        } ?: throw IllegalStateException("Activity must not be null")
    }

    private fun showEvaluationDialog(evaluation: AnswerEvaluation) {
        playEvaluationSound(evaluation)
        val visibleDialog = activity?.let {
            when (evaluation) {
                AnswerEvaluation.SUCCESS -> dialogFactory
                        .createSuccessDialog(it) { whatTimeWillBeViewModel.onEvaluationDialogDismissed() }
                AnswerEvaluation.FAILURE -> dialogFactory
                        .createFailureDialog(it) { whatTimeWillBeViewModel.onEvaluationDialogDismissed() }
            }
        }
        setVisibleDialog(visibleDialog, true)
    }

    private fun showFinishDialog(goodAnswersPercent: Int) {
        playExerciseFinishedSound()
        val visibleDialog = activity?.let {
            when (whatTimeWillBeViewModel.mode) {
                ExerciseMode.LEARN -> dialogFactory
                        .createFinishLearnDialog(it) { whatTimeWillBeViewModel.onEndDialogDismissed(whatTimeWillBeExerciseDescription) }
                ExerciseMode.TEST -> dialogFactory
                        .createFinishTestDialog(it, goodAnswersPercent) { whatTimeWillBeViewModel.onEndDialogDismissed(whatTimeWillBeExerciseDescription) }
            }
        }
        setVisibleDialog(visibleDialog, false)
    }
}