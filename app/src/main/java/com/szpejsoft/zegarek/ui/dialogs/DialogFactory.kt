package com.szpejsoft.zegarek.ui.dialogs

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.szpejsoft.zegarek.R
import com.szpejsoft.zegarek.domain.model.*
import kotlinx.android.synthetic.main.dialog_buttons.view.*
import kotlinx.android.synthetic.main.dialog_evaluation_failure.view.*
import kotlinx.android.synthetic.main.dialog_evaluation_success.view.*
import kotlinx.android.synthetic.main.dialog_exercise_finished.view.*
import kotlinx.android.synthetic.main.dialog_number_selector.view.*
import kotlinx.android.synthetic.main.dialog_rate_app.view.*
import kotlinx.android.synthetic.main.dialog_selector.view.*
import kotlinx.android.synthetic.main.dialog_selector_item.view.*
import java.util.*

class DialogFactory : IDialogFactory {

    private val random = Random()

    override fun createSuccessDialog(activity: Activity, dismissAction: () -> Unit): Dialog {
        val dialogView = View.inflate(activity, R.layout.dialog_evaluation_success, null)
        dialogView.dialogEvaluationSuccessMessage.text = getSuccessText(activity)
        return alertDialog(activity, dialogView, dismissAction)
    }

    override fun createFailureDialog(activity: Activity, dismissAction: () -> Unit): Dialog {
        val dialogView = View.inflate(activity, R.layout.dialog_evaluation_failure, null)
        dialogView.dialogEvaluationFailureMessage.text = getFailureText(activity)
        return alertDialog(activity, dialogView, dismissAction)
    }

    override fun createFinishTestDialog(activity: Activity, scorePercent: Int, dismissAction: () -> Unit): Dialog {
        val dialogView = View.inflate(activity, R.layout.dialog_exercise_finished, null)
        val dialog = alertDialog(activity, dialogView, dismissAction)
        dialogView.dialogExerciseFinishedMessage.text = getTestFinishedText(activity, scorePercent)
        dialogView.dialogExerciseFinishedButton.setOnClickListener { dialog.dismiss() }
        return dialog
    }

    override fun createFinishLearnDialog(activity: Activity, dismissAction: () -> Unit): Dialog {
        val dialogView = View.inflate(activity, R.layout.dialog_exercise_finished, null)
        val dialog = alertDialog(activity, dialogView, dismissAction)
        dialogView.dialogExerciseFinishedMessage.text = activity.getText(R.string.dialog_finished_learn_message)
        dialogView.dialogExerciseFinishedButton.setOnClickListener { dialog.dismiss() }
        return dialog
    }

    override fun createChooseModeDialog(activity: Activity, action: (ExerciseMode) -> Unit): Dialog {
        val dialogView = View.inflate(activity, R.layout.dialog_selector, null)
        val dialog = alertDialog(activity, dialogView, cancelable = true)
        dialogView.selectorDialogTitle.setText(R.string.menu_dialog_choose_mode_title)
        ExerciseMode.values().forEach {
            dialogView.selectorDialogContent.addView(createModeView(it, activity, action, dialog))
        }
        return dialog
    }

    override fun createShowRateAppDialog(activity: Activity, rateLaterAction: () -> Unit, dontShowDialogAction: () -> Unit, rateNowAction: () -> Unit): Dialog {
        val dialogView = View.inflate(activity, R.layout.dialog_rate_app, null)
        val dialog = AlertDialog.Builder(activity)
                .setView(dialogView)
                .setCancelable(false)
                .create()
        dialogView.rateAppDontRateBtn.setOnClickListener {
            dontShowDialogAction.invoke()
            dialog.dismiss()
        }
        dialogView.rateAppRateLaterBtn.setOnClickListener {
            rateLaterAction.invoke()
            dialog.dismiss()
        }
        dialogView.rateAppRateNowBtn.setOnClickListener {
            rateNowAction.invoke()
            dialog.dismiss()
        }
        return dialog
    }

    private fun createModeView(mode: ExerciseMode, activity: Activity, action: (ExerciseMode) -> Unit, dialog: Dialog): View =
            View.inflate(activity, R.layout.dialog_selector_item, null).apply {
                settingsChooserItemTxt.setText(mode.getNameResId())
                setBackgroundResource(R.drawable.white_button_background)
                setOnClickListener {
                    action.invoke(mode)
                    dialog.dismiss()
                }
            }

    override fun createChooseExercisesNumberDialog(activity: Activity, numberSettings: ExercisesNumberSettings, font: Font, action: (Int) -> Unit): Dialog {
        val dialogView = View.inflate(activity, R.layout.dialog_number_selector, null)
        val dialog: AlertDialog = AlertDialog.Builder(activity).setView(dialogView).create()
        val numberPicker = dialogView.chooserDialogNumberPicker
        dialogView.numberSelectorDialogTitle.setText(R.string.settings_dialog_choose_exercises_number_title)
        numberPicker.apply {
            minValue = numberSettings.minimum
            maxValue = numberSettings.maximum
            value = numberSettings.current
            typeface = font.getTypeface(activity)
        }
        dialogView.selectorDialogCancelButton.setOnClickListener { dialog.dismiss() }
        dialogView.selectorDialogOkButton.setOnClickListener {
            action.invoke(numberPicker.value)
            dialog.dismiss()
        }
        return dialog
    }

    override fun createSelectLevelDialog(activity: Activity, currentLevel: Level, action: (Level) -> Unit): Dialog {
        val dialogView = View.inflate(activity, R.layout.dialog_selector, null)
        dialogView.selectorDialogTitle.setText(R.string.settings_dialog_choose_level_title)
        val dialog: AlertDialog = AlertDialog.Builder(activity).setView(dialogView).show()
        Level.values().forEach { level ->
            dialogView.selectorDialogContent.addView(createLevelView(activity, level, action, dialog, level == currentLevel))
        }
        return dialog
    }

    private fun createLevelView(activity: Activity, level: Level, action: (Level) -> Unit, dialog: Dialog, selected: Boolean): View =
            View.inflate(activity, R.layout.dialog_selector_item, null).apply {
                settingsChooserItemTxt.apply {
                    setText(level.getNameResId())
                }
                setBackgroundResource(R.drawable.white_button_background)
                setOnClickListener {
                    action.invoke(level)
                    dialog.dismiss()
                }
                if (selected) {
                    setSelected()
                }
            }

    override fun createSelectFontDialog(activity: Activity, currentFont: Font, action: (Font) -> Unit): Dialog {
        val dialogView = View.inflate(activity, R.layout.dialog_selector, null)
        dialogView.selectorDialogTitle.setText(R.string.settings_dialog_choose_font_title)
        val dialog: AlertDialog = AlertDialog.Builder(activity).setView(dialogView).show()
        Font.values().forEach { font ->
            dialogView.selectorDialogContent.addView(createFontView(activity, font, action, dialog, currentFont == font))
        }
        return dialog
    }

    private fun createFontView(activity: Activity, font: Font, action: (Font) -> Unit, dialog: Dialog, isSelected: Boolean): View =
            View.inflate(activity, R.layout.dialog_selector_item, null).apply {
                settingsChooserItemTxt.apply {
                    setText(font.getNameResId())
                    typeface = ResourcesCompat.getFont(activity, font.getFontResId())
                }
                setBackgroundResource(R.drawable.white_button_background)
                setOnClickListener {
                    action.invoke(font)
                    dialog.dismiss()
                }
                if (isSelected) {
                    setSelected()
                }
            }


    override fun createSelectNumeralsDialog(activity: Activity, currentNumerals: NumeralsType, action: (NumeralsType) -> Unit): Dialog {
        val dialogView = View.inflate(activity, R.layout.dialog_selector, null)
        dialogView.selectorDialogTitle.setText(R.string.settings_dialog_choose_numerals_title)
        val dialog: AlertDialog = AlertDialog.Builder(activity).setView(dialogView).show()
        NumeralsType.values().forEach { numeralsType ->
            dialogView.selectorDialogContent.addView(createNumeralsView(activity, numeralsType, action, dialog, numeralsType == currentNumerals))
        }
        return dialog
    }

    private fun createNumeralsView(activity: Activity, numeralsType: NumeralsType, action: (NumeralsType) -> Unit, dialog: Dialog, isSelected: Boolean): View =
            View.inflate(activity, R.layout.dialog_selector_item, null).apply {
                settingsChooserItemTxt.apply {
                    setText(numeralsType.getNameResId())
                }
                setBackgroundResource(R.drawable.white_button_background)
                setOnClickListener {
                    action.invoke(numeralsType)
                    dialog.dismiss()
                }
                if (isSelected) {
                    setSelected()
                }
            }

    private fun alertDialog(activity: Activity, dialogView: View?, dismissAction: () -> Unit = {}, cancelable: Boolean = false): Dialog {
        return AlertDialog.Builder(activity)
                .setView(dialogView)
                .setCancelable(cancelable)
                .setOnDismissListener { dismissAction.invoke() }
                .create()
    }

    private fun getSuccessText(activity: Activity): String =
            activity.resources.getStringArray(R.array.dialog_success_messages).getRandomString()

    private fun getFailureText(activity: Activity): String =
            activity.resources.getStringArray(R.array.dialog_failure_messages).getRandomString()

    private fun getTestFinishedText(context: Context, scorePercent: Int): String = when (scorePercent) {
        in 0..45 -> context.getString(R.string.dialog_finished_test_grade_e, scorePercent)
        in 46..60 -> context.getString(R.string.dialog_finished_test_grade_d, scorePercent)
        in 61..75 -> context.getString(R.string.dialog_finished_test_grade_c, scorePercent)
        in 76..90 -> context.getString(R.string.dialog_finished_test_grade_b, scorePercent)
        in 91..100 -> context.getString(R.string.dialog_finished_test_grade_a, scorePercent)
        else -> throw IllegalArgumentException("scorePercent must be in [0,100]")
    }

    private fun Array<String>.getRandomString(): String = this[random.nextInt(this.size)]

    private fun View.setSelected() = setBackgroundColor(ContextCompat.getColor(context, R.color.primaryLight))

}