package com.szpejsoft.zegarek.ui.main

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.MenuItem
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import com.szpejsoft.zegarek.R
import com.szpejsoft.zegarek.domain.model.getThemeResId
import com.szpejsoft.zegarek.ui.music.MusicActivity
import kotlinx.android.synthetic.main.main_activity.*
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : MusicActivity() {
    private val mainActivityViewModel: IMainActivityViewModel by viewModel<MainActivityViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        requestedOrientation = if (isPhone()) ActivityInfo.SCREEN_ORIENTATION_PORTRAIT else ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        setTheme(mainActivityViewModel.getFont().getThemeResId())
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        setSupportActionBar(mainActivityToolbar)
        setupActionBar()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item,
                Navigation.findNavController(this, R.id.navHostFragment))
                || super.onOptionsItemSelected(item)
    }

    private fun setupActionBar() {
        val host: NavHostFragment = supportFragmentManager
                .findFragmentById(R.id.navHostFragment) as NavHostFragment? ?: return
        val navController = host.navController

        NavigationUI.setupActionBarWithNavController(this, navController, null)
    }

    override fun onSupportNavigateUp(): Boolean {
        return NavigationUI.navigateUp(Navigation.findNavController(this, R.id.navHostFragment), null)
    }

}
