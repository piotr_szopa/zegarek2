package com.szpejsoft.zegarek.ui.whattimewillbe

import android.view.View
import androidx.lifecycle.LiveData
import com.szpejsoft.zegarek.domain.model.AnswerEvaluation
import com.szpejsoft.zegarek.domain.model.ExerciseMode
import com.szpejsoft.zegarek.domain.model.exercises.WhatTimeWillBeExercise

interface IWhatTimeWillBeViewModel {
    val exercisesInSetData: LiveData<Int>
    val exercisesLeftData: LiveData<Int>
    val exerciseData: LiveData<WhatTimeWillBeExercise>
    val evaluationData: LiveData<AnswerEvaluation>
    val finishData: LiveData<Int>

    var mode: ExerciseMode

    fun onAnswerClicked(answerOrdinal: Int)
    fun onEvaluationDialogDismissed()
    fun onEndDialogDismissed(view: View)
}