package com.szpejsoft.zegarek.ui.splash

import android.animation.TimeInterpolator
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.DecelerateInterpolator
import androidx.constraintlayout.widget.ConstraintSet
import androidx.lifecycle.Observer
import androidx.transition.Transition
import androidx.transition.TransitionInflater
import androidx.transition.TransitionManager
import com.szpejsoft.zegarek.R
import com.szpejsoft.zegarek.ui.base.BaseFragment
import com.szpejsoft.zegarek.ui.main.MainActivity
import kotlinx.android.synthetic.main.splash_fragment_start.*
import org.koin.android.viewmodel.ext.android.viewModel


class SplashFragment : BaseFragment() {

    private val splashFragmentViewModel: ISplashFragmentViewModel by viewModel<SplashFragmentViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.splash_fragment_start, container, false)

    override fun bindViewModel() {
        splashFragmentViewModel.init()
        splashFragmentViewModel.showAnimationLiveData.observe(viewLifecycleOwner, Observer { showAnimation() })
    }

    private fun showAnimation() {
        activity?.let { act ->
            val endConstraintSet = ConstraintSet()
            endConstraintSet.clone(act, R.layout.splash_fragment_end)
            val interpolator = DecelerateInterpolator(1.0f)
            val transition = getTransition(R.transition.logo_transition, interpolator, act)
            transition.addListener(TransitionListener { navigateToMenu() })
            TransitionManager.beginDelayedTransition(splashRoot, transition)
            endConstraintSet.applyTo(splashRoot)
        } ?: throw IllegalArgumentException("activity MUST not be null")
    }

    private fun navigateToMenu() {
        activity?.let { act ->
            val intent = Intent(act, MainActivity::class.java)
            act.startActivity(intent)
            act.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            act.finish()
        }
    }


    private fun getTransition(transitionResId: Int, timeInterpolator: TimeInterpolator, activity: Activity): Transition {
        return TransitionInflater.from(activity)
                .inflateTransition(transitionResId)
                .apply {
                    interpolator = timeInterpolator
                    duration = 2000
                    startDelay = 0
                }
    }


}
