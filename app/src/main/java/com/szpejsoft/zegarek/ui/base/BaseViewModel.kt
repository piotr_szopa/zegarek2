package com.szpejsoft.zegarek.ui.base

import android.app.Application
import android.os.Build
import android.text.Html
import android.text.Spanned
import androidx.annotation.StringRes
import androidx.lifecycle.AndroidViewModel
import com.szpejsoft.zegarek.base.SchedulersFacade
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.plusAssign
import timber.log.Timber
import java.util.*

abstract class BaseViewModel(application: Application,
                             private val schedulersFacade: SchedulersFacade
) : AndroidViewModel(application) {
    private val compositeDisposable = CompositeDisposable()
    private val random: Random = Random()

    override fun onCleared() {
        super.onCleared()
        clear()
    }

    fun <T> Single<T>.subscribeBy(
            onError: (Throwable) -> Unit = Timber::w,
            onSuccess: (T) -> Unit = {}
    ): Disposable {
        val subscribe = subscribeOn(schedulersFacade.background())
                .observeOn(schedulersFacade.ui())
                .subscribe(onSuccess, onError)
        compositeDisposable += subscribe
        return subscribe
    }

    fun <T> Observable<T>.subscribeBy(
            onError: (Throwable) -> Unit = Timber::w,
            onNext: (T) -> Unit = {}
    ): Disposable {
        val subscribe = subscribeOn(schedulersFacade.background())
                .observeOn(schedulersFacade.ui())
                .subscribe(onNext, onError)
        compositeDisposable += subscribe
        return subscribe
    }

    fun Completable.subscribeBy(
            onError: (Throwable) -> Unit = Timber::w,
            onComplete: () -> Unit
    ): Disposable {
        val subscribe = subscribeOn(schedulersFacade.background())
                .observeOn(schedulersFacade.ui())
                .subscribe(onComplete, onError)
        compositeDisposable += subscribe
        return subscribe
    }

    protected fun nextInt(bound: Int): Int = random.nextInt(bound)

    private fun clear() = compositeDisposable.clear()

    @Suppress("DEPRECATION")
    protected fun getSpannable(@StringRes stringId: Int): Spanned {
        val html = getApplication<Application>().getString(stringId)
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY)
        } else {
            Html.fromHtml(html)
        }
    }

}
