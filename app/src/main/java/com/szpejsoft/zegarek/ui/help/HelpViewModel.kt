package com.szpejsoft.zegarek.ui.help

import android.app.Application
import android.text.Spanned
import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.szpejsoft.zegarek.R
import com.szpejsoft.zegarek.base.SchedulersFacade
import com.szpejsoft.zegarek.domain.model.exercises.ChosenExercise
import com.szpejsoft.zegarek.ui.base.BaseViewModel

class HelpViewModel(application: Application,
                    schedulersFacade: SchedulersFacade
) : BaseViewModel(application, schedulersFacade), IHelpViewModel {

    override val helpData = MutableLiveData<Spanned>()

    override fun setChosenExercise(exercise: ChosenExercise) {
        val stringId = getHelpStringId(exercise)
        helpData.postValue(getSpannable(stringId))
    }


    @StringRes
    private fun getHelpStringId(exercise: ChosenExercise): Int =
            when (exercise) {
                ChosenExercise.WHAT_TIME_IT_IS -> R.string.help_what_time_it_is
                ChosenExercise.ADJUST_CLOCK -> R.string.help_adjust_clock
                ChosenExercise.TIME_PASSED -> R.string.help_how_much_time_passed
                ChosenExercise.WHAT_TIME_WILL_BE -> R.string.help_what_time_will_be
            }

}