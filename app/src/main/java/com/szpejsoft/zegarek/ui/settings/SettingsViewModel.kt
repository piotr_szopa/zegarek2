package com.szpejsoft.zegarek.ui.settings

import android.app.Activity
import android.app.Application
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.navigation.Navigation
import com.szpejsoft.zegarek.R
import com.szpejsoft.zegarek.base.SchedulersFacade
import com.szpejsoft.zegarek.data.exercises.IExercisesNumberRepository
import com.szpejsoft.zegarek.data.font.IFontRepository
import com.szpejsoft.zegarek.data.level.ILevelRepository
import com.szpejsoft.zegarek.data.music.IMusicRepository
import com.szpejsoft.zegarek.data.numerals.INumeralsRepository
import com.szpejsoft.zegarek.domain.model.ExercisesNumberSettings
import com.szpejsoft.zegarek.domain.model.Font
import com.szpejsoft.zegarek.domain.model.Level
import com.szpejsoft.zegarek.domain.model.NumeralsType
import com.szpejsoft.zegarek.ui.base.BaseViewModel

class SettingsViewModel(application: Application,
                        schedulersFacade: SchedulersFacade,
                        private val exercisesRepository: IExercisesNumberRepository,
                        private val fontRepository: IFontRepository,
                        private val levelRepository: ILevelRepository,
                        private val musicRepository: IMusicRepository,
                        private val numeralsRepository: INumeralsRepository
) : BaseViewModel(application, schedulersFacade), ISettingsViewModel {
    override val exercisesNumberLiveData = MutableLiveData<ExercisesNumberSettings>()
    override val fontLiveData = MutableLiveData<Font>()
    override val levelLiveData = MutableLiveData<Level>()
    override val musicOnLiveData = MutableLiveData<Boolean>()
    override val numeralsLiveData = MutableLiveData<NumeralsType>()
    override val soundOnLiveData = MutableLiveData<Boolean>()

    init {
        musicRepository.musicStateObservable.subscribeBy { musicOnLiveData.value = it }
        musicRepository.soundStateObservable.subscribeBy { soundOnLiveData.value = it }
        numeralsRepository.numeralsTypeObservable.subscribeBy { numeralsLiveData.value = it }
        fontRepository.fontObservable.subscribeBy { fontLiveData.value = it }
        levelRepository.observeLevel.subscribeBy { levelLiveData.value = it }
        exercisesRepository.observeExercises.subscribeBy { exercisesNumberLiveData.value = it }
    }

    override fun onMusicSwitched(musicOn: Boolean) = musicRepository.saveMusicState(musicOn)

    override fun onSoundSwitched(soundOn: Boolean) = musicRepository.saveSoundState(soundOn)

    override fun onLevelChosen(level: Level) = levelRepository.saveLevel(level)

    override fun onNumeralsChosen(type: NumeralsType) = numeralsRepository.saveNumeralsType(type)

    override fun onExercisesNumberChosen(exercisesNumber: Int) = exercisesRepository.saveExercisesNumber(exercisesNumber)

    override fun onAboutClicked(aboutButton: View) {
        Navigation.findNavController(aboutButton).navigate(R.id.settings_to_aboutFragment)
    }

    override fun onFontChosen(font: Font, activity: Activity) {
        fontRepository.saveFont(font)
        activity.recreate()
    }
}