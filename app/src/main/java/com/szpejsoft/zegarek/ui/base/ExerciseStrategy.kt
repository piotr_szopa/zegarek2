package com.szpejsoft.zegarek.ui.base

import androidx.lifecycle.MutableLiveData
import com.szpejsoft.zegarek.domain.model.AnswerEvaluation

sealed class ExerciseStrategy<T> {
    abstract fun handleWrongAnswer(exerciseList: MutableList<T>,
                                   currentExercise: T,
                                   finishData: MutableLiveData<Int>,
                                   evaluationData: MutableLiveData<AnswerEvaluation>)

    abstract fun handleProperAnswer(exerciseList: MutableList<T>,
                                    currentExercise: T,
                                    finishData: MutableLiveData<Int>,
                                    evaluationData: MutableLiveData<AnswerEvaluation>)
}

class LearnStrategy<T> : ExerciseStrategy<T>() {
    override fun handleWrongAnswer(exerciseList: MutableList<T>,
                                   currentExercise: T,
                                   finishData: MutableLiveData<Int>,
                                   evaluationData: MutableLiveData<AnswerEvaluation>) {
        evaluationData.value = AnswerEvaluation.FAILURE
    }

    override fun handleProperAnswer(exerciseList: MutableList<T>,
                                    currentExercise: T,
                                    finishData: MutableLiveData<Int>,
                                    evaluationData: MutableLiveData<AnswerEvaluation>) {
        exerciseList.remove(currentExercise)
        if (exerciseList.isEmpty()) {
            finishData.value = 100
        } else {
            evaluationData.value = AnswerEvaluation.SUCCESS
        }
    }

    override fun equals(other: Any?): Boolean {
        return this === other
    }

    override fun hashCode(): Int {
        return System.identityHashCode(this)
    }
}

class TestStrategy<T> : ExerciseStrategy<T>() {
    var exercises: Int = 0
    private var properAnswers: Int = 0
    override fun handleWrongAnswer(exerciseList: MutableList<T>,
                                   currentExercise: T,
                                   finishData: MutableLiveData<Int>,
                                   evaluationData: MutableLiveData<AnswerEvaluation>) {
        exerciseList.remove(currentExercise)
        if (exerciseList.isEmpty()) {
            finishData.value = 100
        } else {
            evaluationData.value = AnswerEvaluation.FAILURE
        }
    }

    override fun handleProperAnswer(exerciseList: MutableList<T>,
                                    currentExercise: T,
                                    finishData: MutableLiveData<Int>,
                                    evaluationData: MutableLiveData<AnswerEvaluation>) {
        exerciseList.remove(currentExercise)
        properAnswers++
        if (exerciseList.isEmpty()) {
            finishData.value = (properAnswers * 100 / exercises)
        } else {
            evaluationData.value = AnswerEvaluation.SUCCESS
        }
    }

    override fun equals(other: Any?): Boolean {
        return this === other
    }

    override fun hashCode(): Int {
        return System.identityHashCode(this)
    }
}