package com.szpejsoft.zegarek.koin

import com.szpejsoft.zegarek.base.SchedulersFacade
import com.szpejsoft.zegarek.base.ClockSchedulersFacade
import com.szpejsoft.zegarek.data.adjustclock.AdjustClockRepository
import com.szpejsoft.zegarek.data.adjustclock.IAdjustClockRepository
import com.szpejsoft.zegarek.data.common.ClockDB
import com.szpejsoft.zegarek.data.common.IClockDB
import com.szpejsoft.zegarek.data.exercises.ExercisesNumberDao
import com.szpejsoft.zegarek.data.exercises.ExercisesNumberRepository
import com.szpejsoft.zegarek.data.exercises.IExercisesNumberDao
import com.szpejsoft.zegarek.data.exercises.IExercisesNumberRepository
import com.szpejsoft.zegarek.data.font.FontDao
import com.szpejsoft.zegarek.data.font.FontRepository
import com.szpejsoft.zegarek.data.font.IFontDao
import com.szpejsoft.zegarek.data.font.IFontRepository
import com.szpejsoft.zegarek.data.level.ILevelDao
import com.szpejsoft.zegarek.data.level.ILevelRepository
import com.szpejsoft.zegarek.data.level.LevelDao
import com.szpejsoft.zegarek.data.level.LevelRepository
import com.szpejsoft.zegarek.data.music.IMusicDao
import com.szpejsoft.zegarek.data.music.IMusicRepository
import com.szpejsoft.zegarek.data.music.MusicDao
import com.szpejsoft.zegarek.data.music.MusicRepository
import com.szpejsoft.zegarek.data.numerals.INumeralsDao
import com.szpejsoft.zegarek.data.numerals.INumeralsRepository
import com.szpejsoft.zegarek.data.numerals.NumeralsDao
import com.szpejsoft.zegarek.data.numerals.NumeralsRepository
import com.szpejsoft.zegarek.data.rateapp.IRateAppDao
import com.szpejsoft.zegarek.data.rateapp.IRateAppRepository
import com.szpejsoft.zegarek.data.rateapp.RateAppDao
import com.szpejsoft.zegarek.data.rateapp.RateAppRepository
import com.szpejsoft.zegarek.data.timepassed.ITimePassedRepository
import com.szpejsoft.zegarek.data.timepassed.TimePassedRepository
import com.szpejsoft.zegarek.data.whattimeitis.IWhatTimeItIsRepository
import com.szpejsoft.zegarek.data.whattimeitis.WhatTimeItIsRepository
import com.szpejsoft.zegarek.data.whattimewillbe.IWhatTimeWillBeRepository
import com.szpejsoft.zegarek.data.whattimewillbe.WhatTimeWillBeRepository
import com.szpejsoft.zegarek.domain.usecase.adjustclock.AdjustClockUseCase
import com.szpejsoft.zegarek.domain.usecase.adjustclock.IAdjustClockUseCase
import com.szpejsoft.zegarek.domain.usecase.timepassed.ITimePassedUseCase
import com.szpejsoft.zegarek.domain.usecase.timepassed.TimePassedUseCase
import com.szpejsoft.zegarek.domain.usecase.whattimeitis.IWhatTimeItIsUseCase
import com.szpejsoft.zegarek.domain.usecase.whattimeitis.WhatTimeItIsUseCase
import com.szpejsoft.zegarek.domain.usecase.whattimewillbe.IWhatTimeWillBeUseCase
import com.szpejsoft.zegarek.domain.usecase.whattimewillbe.WhatTimeWillBeUseCase
import com.szpejsoft.zegarek.ui.about.AboutViewModel
import com.szpejsoft.zegarek.ui.adjustclock.AdjustClockViewModel
import com.szpejsoft.zegarek.ui.dialogs.DialogFactory
import com.szpejsoft.zegarek.ui.dialogs.IDialogFactory
import com.szpejsoft.zegarek.ui.help.HelpViewModel
import com.szpejsoft.zegarek.ui.main.MainActivityViewModel
import com.szpejsoft.zegarek.ui.menu.MenuViewModel
import com.szpejsoft.zegarek.ui.music.MusicActivityViewModel
import com.szpejsoft.zegarek.ui.settings.SettingsViewModel
import com.szpejsoft.zegarek.ui.splash.SplashFragmentViewModel
import com.szpejsoft.zegarek.ui.timepassed.TimePassedViewModel
import com.szpejsoft.zegarek.ui.whattimeitis.WhatTimeItIsViewModel
import com.szpejsoft.zegarek.ui.whattimewillbe.WhatTimeWillBeViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val databaseModule = module {
    single<IClockDB> { ClockDB(get()) }
    single<IExercisesNumberDao> { ExercisesNumberDao(get()) }
    single<IFontDao> { FontDao(get()) }
    single<ILevelDao> { LevelDao(get()) }
    single<IMusicDao> { MusicDao(get()) }
    single<INumeralsDao> { NumeralsDao(get()) }
    single<IRateAppDao> { RateAppDao(get()) }
}

val utilsModule = module {
    single<IDialogFactory> { DialogFactory() }
}

val repositoryModule = module {
    single<IAdjustClockRepository> { AdjustClockRepository() }
    single<IExercisesNumberRepository> { ExercisesNumberRepository(get()) }
    single<IFontRepository> { FontRepository(get()) }
    single<ILevelRepository> { LevelRepository(get()) }
    single<IMusicRepository> { MusicRepository(get()) }
    single<INumeralsRepository> { NumeralsRepository(get()) }
    single<IRateAppRepository> { RateAppRepository(get()) }
    single<ITimePassedRepository> { TimePassedRepository() }
    single<IWhatTimeItIsRepository> { WhatTimeItIsRepository() }
    single<IWhatTimeWillBeRepository> { WhatTimeWillBeRepository() }
}

val schedulersModule = module {
    single<SchedulersFacade> { ClockSchedulersFacade() }
}

val useCaseModule = module {
    single<IAdjustClockUseCase> { AdjustClockUseCase(get(), get(), get(), get(), get()) }
    single<ITimePassedUseCase> { TimePassedUseCase(get(), get(), get()) }
    single<IWhatTimeItIsUseCase> { WhatTimeItIsUseCase(get(), get(), get(), get(), get()) }
    single<IWhatTimeWillBeUseCase> { WhatTimeWillBeUseCase(get(), get(), get()) }
}

val viewModelModule = module {
    viewModel { AboutViewModel(get(), get()) }
    viewModel { AdjustClockViewModel(get(), get(), get()) }
    viewModel { HelpViewModel(get(), get()) }
    viewModel { MainActivityViewModel(get(), get(), get()) }
    viewModel { MusicActivityViewModel(get(), get(), get()) }
    viewModel { MenuViewModel(get(), get(), get()) }
    viewModel { SettingsViewModel(get(), get(), get(), get(), get(), get(), get()) }
    viewModel { SplashFragmentViewModel(get(), get()) }
    viewModel { TimePassedViewModel(get(), get(), get()) }
    viewModel { WhatTimeItIsViewModel(get(), get(), get()) }
    viewModel { WhatTimeWillBeViewModel(get(), get(), get()) }

}